# Ciceksepeti Hackathon Source Files

* Kod Java 8 ile yazıldı. 
* IDE olarak IntelliJ IDEA kullanıldı. 
* Dependency'ler için Maven projesi olarak oluşturuldu.
* Maven projesi olduğu için doğrudan .JAR uzantılı dosyayı yükledik. Kodu çalıştırmak için aşağıdaki komutların çalıştırılması gerekiyor:

~~~~
>_ java -jar ciceksepeti_hackathon-1.0-SNAPSHOT-jar-with-dependencies.jar
~~~~

* Bu şekilde bayiler (retailer), siparisler (order) ve rotalar siparis numaralarıyla Terminal üzerinde görülebilir. Sonuçlar 'scripts' klasörü altında 'assignedOrders.csv' dosyasına kaydediliyor. Sonrasında 2D düzlem üzerinde noktaları ve rotayı görüntülemek için aşağıdaki komutlar çalıştırılmalıdır:

~~~~
>_ cd scripts
>_ python showMap.py
~~~~

* Java kısmında Genetik Algoritma ve Travelling Salesman Problemi için "Nearest Neighbor" yöntemini kullandık. Hepimiz farklı işlerde çalıştığımızdan Java tarafında 2D düzlem üzerinde görüntülemeyi tamamlayamadık, Python'da daha önceden hazırlamış olduğumuz kod ile noktalar görüntülenebilir.

Görkem Özer, Emre Külah, Semih Aktaş
