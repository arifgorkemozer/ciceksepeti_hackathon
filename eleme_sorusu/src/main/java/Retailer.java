
import java.util.*;

/**
 * Distributor for the orders assigned to it.
 */
public class Retailer {

    /**
     * Name of the retailer (red, green, blue).
     */
    private String name;

    /**
     * Latitude of the retailer.
     */
    private double lat;

    /**
     * Longitude of the retailer.
     */
    private double lng;

    private int minQuota;
    private int maxQuota;
    private List<Order> route;
    private List<Order> orders;

    public Retailer(String name, double lat, double lng, int minQuota, int maxQuota) {
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.minQuota = minQuota;
        this.maxQuota = maxQuota;
        this.route = new ArrayList<>();
        this.orders = new ArrayList<>();
    }

    public Retailer(Retailer retailer) {
        this.name = retailer.getName();
        this.lat = retailer.getLat();
        this.lng = retailer.getLng();
        this.minQuota = retailer.getMinQuota();
        this.maxQuota = retailer.getMaxQuota();
        this.route = new ArrayList<>();
        this.orders = new ArrayList<>();

        for (Order order : retailer.getOrders()) {
            this.orders.add(order);
        }
    }

    public void setOrders(Collection<Order> orderCollection) {
        this.orders = new ArrayList<>();

        for (Order order : orderCollection) {
            this.orders.add(order);
        }

    }

    public List<Order> getOrders() {
        return this.orders;
    }

    public void clearOrders() {
        this.orders = new ArrayList<>();
    }

    public boolean isValid() {
        int orderCount = this.orders.size();

        return orderCount >= this.minQuota && orderCount <= this.maxQuota;
    }

    public boolean canAddOrder() {
        return this.orders.size() < this.maxQuota;
    }

    public boolean canRemoveOrder() {
        return this.orders.size() > this.minQuota;
    }

    public int getOrderCount() {
        return this.orders.size();
    }

    public int getMinQuota() {
        return this.minQuota;
    }

    public void setMinQuota(int minQuota) {
        this.minQuota = minQuota;
    }

    public int getMaxQuota() {
        return this.maxQuota;
    }

    public void setMaxQuota(int maxQuota) {
        this.maxQuota = maxQuota;
    }


    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return this.lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return this.lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "Retailer{"
                + "name='" + name + '\''
                + ", lat=" + lat
                + ", lng=" + lng
                + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Retailer retailer = (Retailer) o;
        return Double.compare(retailer.lat, lat) == 0
                && Double.compare(retailer.lng, lng) == 0
                && Objects.equals(name, retailer.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lat, lng);
    }

    /**
     * @param order Order object to be added.
     */
    public void addOrder(Order order) {
        this.orders.add(order);
    }

    public void addOrderToIndex(int index, Order order) {
        List<Order> clone = Utils.copyOrderList(this.orders);
        clone.add(index, order);

        this.setOrders(clone);
    }

    /**
     * @param order Order object to be removed.
     */
    public boolean removeOrder(Order order) {
        return this.orders.remove(order);
    }

    /**
     * @param orderNumber Order number of the order to be removed.
     */
    public void removeOrderByOrderNumber(int orderNumber) {
        for (Order order : this.orders) {
            if (order.getOrderNumber() == orderNumber) {
                this.removeOrder(order);
                return;
            }
        }
    }

    /**
     * @param orderCollection
     * @return Number of deleted elements.
     */
    public int removeMultipleOrders(Collection<Order> orderCollection) {
        int deletedOrderCount = 0;

        for (Order order : orderCollection) {
            boolean deletionResult = this.removeOrder(order);

            if (deletionResult) {
                deletedOrderCount++;
            }
        }

        return deletedOrderCount;
    }

    public void addMultipleOrdersToIndex(int index, Collection<Order> orderCollection) {

        List<Order> clone = Utils.copyOrderList(this.orders);
        int relativeIndex = 0;

        for (Order order : orderCollection) {
            clone.add(index + relativeIndex, order);
            relativeIndex++;
        }

        this.setOrders(clone);
    }

    public List<Order> getRoute() {
        return route;
    }

    public void setRoute(List<Order> route) {
        this.route = Utils.copyOrderList(route);
    }

    public void printRoute() {
        if (this.route != null) {
            // print retailer as start
            System.out.println(this.getLat() + " " + this.getLng());

            for (int i = 0; i < this.route.size(); i++) {
                Order order = this.route.get(i);
                System.out.println(order.getLat() + " " + order.getLng());
            }

            // print retailer as final
            System.out.println(this.getLat() + " " + this.getLng());

        } else {
            System.out.println("Route is not calculated yet!");
        }
    }
}
