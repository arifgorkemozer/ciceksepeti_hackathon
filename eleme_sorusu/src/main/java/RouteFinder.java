

import java.util.*;

public class RouteFinder {


    public Retailer retailer;
    public List<Route> population;
    public int MAX_POP_SIZE;
    public int MAX_ITERATION_COUNT;

    public RouteFinder(Retailer retailer, int max_pop_size, int max_iteration_count) {
        this.retailer = retailer;
        this.population = new ArrayList<>();
        this.MAX_POP_SIZE = max_pop_size;
        this.MAX_ITERATION_COUNT = max_iteration_count;
    }

    public Route generateRandom() {
        Random rand = new Random();
        List<Order> clone = Utils.copyOrderList(this.retailer.getOrders());
        List<Order> result = new ArrayList<>();

        // clone.addAll(this.retailer.getOrders());

        for (int i = 0; i < this.retailer.getOrderCount(); i++) {
            int randIndex = rand.nextInt(clone.size());
            result.add(clone.get(randIndex));
            clone.remove(randIndex);
        }

        return new Route(this.retailer, result);
    }

    public Route mutate(Route r) {
        Random rand = new Random();
        List<Order> result = Utils.copyOrderList(r.route);

        int ind1 = rand.nextInt(r.route.size());
        int ind2 = -1;
        int lg, sm;

        while (ind2 == -1 || ind2 == ind1) {
            ind2 = rand.nextInt(r.route.size());
        }

        lg = ind1 > ind2 ? ind1 : ind2;
        sm = ind1 < ind2 ? ind1 : ind2;

        Order o1 = result.get(sm);
        Order o2 = result.get(lg);

        result.remove(lg);
        result.remove(sm);

        result.add(sm, o2);
        result.add(lg, o1);

        return new Route(r.retailer, result);
    }

    public Route[] crossover(Route r1, Route r2) {

        List<Order> crossoverResult = new ArrayList<>();
        List<Order> r1p1 = new ArrayList<>();
        List<Order> r1p2 = Utils.copyOrderList(r1.route);
        List<Order> r2p1 = Utils.copyOrderList(r2.route);
        List<Order> r2p2 = Utils.copyOrderList(r2.route);

        for (int i = 0; i < r1.route.size() / 2; i++) {
            r1p1.add(r1.route.get(i));
        }

        r1p2.removeAll(r1p1);
        r2p1.removeAll(r1p1);
        r2p2.removeAll(r1p2);

        List<Order> result1 = Utils.copyOrderList(r1p1);
        result1.addAll(r2p1);

        List<Order> result2 = Utils.copyOrderList(r1p2);
        result2.addAll(r2p2);

        return new Route[] {new Route(r1.retailer, result1), new Route(r1.retailer, result2)};
    }

    public void doMutations() {
        Random rand = new Random();
        float prob;
        int randIndex;
        for (int i = 0; i < MAX_POP_SIZE/5; i++) {
            prob = rand.nextFloat();
            if (prob < 0.1) {
                randIndex = rouletteWheelSelection();
                Route child = mutate(population.get(randIndex));
                if (child != null) {
                    population.add(child);
                }
            }
        }
    }

    public void doCrossovers() {
        Random rand = new Random();
        float prob;
        int randIndex1;
        int randIndex2 = -1;
        for (int i = 0; i < MAX_POP_SIZE/3; i++) {
            prob = rand.nextFloat();
            if (prob < 0.1) {
                randIndex1 = rouletteWheelSelection();

                // continue selecting until new distinct index found
                while (randIndex2 == -1 || randIndex2 == randIndex1) {
                    randIndex2 = rouletteWheelSelection();
                }

                Route[] children = crossover(population.get(randIndex1), population.get(randIndex2));

                population.add(children[0]);
                population.add(children[1]);
            }
        }
    }

    public void sortPopulation() {
        Collections.sort(population, new Comparator<Route>() {
            @Override
            public int compare(Route lhs, Route rhs) {
                // 1 - less than, -1 - greater than, 0 - equal, all inversed for descending
                return lhs.getFitness() > rhs.getFitness() ? 1 : (lhs.getFitness() < rhs.getFitness()) ? -1 : 0;
            }
        });
    }

    public int rouletteWheelSelection() {
        Random rand = new Random();
        float random = rand.nextFloat();
        if (random < 0.6) {
            return rand.nextInt(population.size() / 10);
        } else if (random < 0.8) {
            return rand.nextInt(population.size() / 3);
        } else {
            return rand.nextInt(population.size());
        }
    }

    public void naturalSelection() {
        Random rand = new Random();
        sortPopulation();

        /*
        Select 3 from top %10 of population. Kill them by probability of %10.
        */
        for (int i = 0; i < 3; i++) {
            if (population.size() > MAX_POP_SIZE) {
                if (rand.nextFloat() < 0.1) {
                    int ind = rand.nextInt(10);
                    population.remove(ind);
                }
            }
        }

        int range = 0;
        int ind = 0;
        while (population.size() > MAX_POP_SIZE) {
            if (rand.nextFloat() < 0.6) {
                range = population.size() - 10;
                ind = 10 + rand.nextInt(range);

                population.remove(ind);
            }
        }

        for (int i = population.size() - MAX_POP_SIZE/10; i < population.size(); i++) {
            population.remove(i);
        }

        for (int i = 0; i < MAX_POP_SIZE/10; i++) {
            population.add(generateRandom());
        }

        sortPopulation();
    }

    private void getTopFitnessScores() {
        sortPopulation();
        for (int i = 0; i < 3; i++) {
            System.out.println("Top - " + i + ": " + population.get(i).getFitness());
        }
    }

    public Route getBest() {
        sortPopulation();
        return this.population.get(0);
    }

    public double getMinFitness() {
        sortPopulation();
        return this.population.get(0).getFitness();
    }

    public Route run() {
        for (int i = 0; i < MAX_POP_SIZE; i++) {
            this.population.add(generateRandom());
        }

        int iterationNumber = 0;

        while (iterationNumber < MAX_ITERATION_COUNT) {
            sortPopulation();
            doMutations();
            doCrossovers();
            naturalSelection();

//            System.out.println("Iteration Count: " + iterationNumber + "\n");
//            getTopFitnessScores();
//            System.out.println("Population size : " + population.size());

            iterationNumber++;
        }
        return getBest();
    }


}
