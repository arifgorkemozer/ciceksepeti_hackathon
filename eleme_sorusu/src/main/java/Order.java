import java.util.Objects;

/**
 * An order class for CicekSepeti orders.
 */
public class Order {

    /**
     * Distinct number for the order.
     */
    private int orderNumber;

    /**
     * Latitude of the order.
     */
    private double lat;

    /**
     * Latitude of the order.
     */
    private double lng;

    public Order(int orderNumber, double lat, double lng) {
        this.orderNumber = orderNumber;
        this.lat = lat;
        this.lng = lng;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderNumber=" + orderNumber +
                ", lat=" + lat +
                ", lng=" + lng +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderNumber == order.orderNumber &&
                Double.compare(order.lat, lat) == 0 &&
                Double.compare(order.lng, lng) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderNumber, lat, lng);
    }
}
