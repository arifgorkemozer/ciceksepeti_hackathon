/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author semih
 */
public class Position {
    private String id;
    private double latitude;
    private double longitude;

    Position(String id, double latitude, double longitude){
        this.setId(id);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    
    @Override
    public String toString(){
        return "Point(id: " + this.id 
                + ", latitude: " + this.latitude 
                + ", longitude: " + this.longitude
                + ")";
    }
}
