
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Main {

    public static double TSP_STARTING_TEMP = 100;
    public static int TSP_ITERATION_COUNT = 10000;
    public static double TSP_COOLING_RATE = 0.9995;
    public static int POPULATION_SIZE = 200;

    public static List<Retailer> ALL_RETAILERS = new ArrayList<>();
    public static List<Order> ALL_ORDERS = new ArrayList<>();

    public static void main(String args[]) throws Exception {
        String colors[] = {"Kirmizi", "Yesil", "Mavi"};
        int minQuotas[] = {20, 35, 20};
        int maxQuotas[] = {30, 50, 80};

        String path = "siparis ve bayi koordinatları.xlsx";

        XSSFWorkbook wb = Utils.read_xlsx(path);
        List<Position> orderPosList = Utils.read_sheet(wb.getSheetAt(0));
        List<Position> retailerPosList = Utils.read_sheet(wb.getSheetAt(1));

//        double minMaxValues[] = Utils.getMinMax(orderPosList, retailerPosList);
//        Utils.scalePositions(orderPosList, retailerPosList, minMaxValues, 100);

        for (Position pos : orderPosList) {
            ALL_ORDERS.add(new Order(Integer.valueOf(pos.getId()), pos.getLatitude(), pos.getLongitude()));
        }

        for (int i = 0; i < retailerPosList.size(); i++) {
            Position pos = retailerPosList.get(i);
            ALL_RETAILERS.add(new Retailer(pos.getId(), pos.getLatitude(), pos.getLongitude(), minQuotas[i], maxQuotas[i]));
        }

        List<Configuration> confs = new ArrayList<>();

        // generate random configurations
        for (int i = 0; i < 170; i++) {
            Configuration conf = Configuration.generateRandom(ALL_RETAILERS, ALL_ORDERS);
            confs.add(conf);
            if (!conf.isValid()) {
                throw new Exception("Conf is not valid!\n" + conf);
            }
        }

        // generate configurations with k-means
        while (confs.size() < 190) {
            Configuration conf = Configuration.generateWithKmeans(ALL_ORDERS, false);
            if (conf.isValid()) {
                confs.add(conf);
            }
        }



        // generate configurations with k-means
        while (confs.size() < 200) {
            Configuration conf = Configuration.generateWithKmeans(ALL_ORDERS, true);
            if (conf.isValid()) {
                confs.add(conf);
            }
        }

        GeneticAlgorithm GA = new GeneticAlgorithm(confs, 0, 3000);
        Configuration conf = GA.runAlgorithm();

//        /* GENETIC ALGO OPTIMIZATION OF ORDERS AT THE END */
//        Route oldRoute1 = new Route(conf.getRetailers().get(0), conf.getRetailers().get(0).getOrders());
//        Route oldRoute2 = new Route(conf.getRetailers().get(1), conf.getRetailers().get(1).getOrders());
//        Route oldRoute3 = new Route(conf.getRetailers().get(2), conf.getRetailers().get(2).getOrders());
//
//        System.out.println(conf);
//        System.out.println(oldRoute1.getFitness());
//        System.out.println(oldRoute2.getFitness());
//        System.out.println(oldRoute3.getFitness());
//        System.out.println("---------------");
//
//        RouteFinder routeFinder1 = new RouteFinder(conf.getRetailers().get(0), 200, 10000);
//        RouteFinder routeFinder2 = new RouteFinder(conf.getRetailers().get(1), 200, 10000);
//        RouteFinder routeFinder3 = new RouteFinder(conf.getRetailers().get(2), 200, 10000);
//
//        Route newRoute1 = routeFinder1.run();
//        Route newRoute2 = routeFinder2.run();
//        Route newRoute3 = routeFinder3.run();
//
//        System.out.println(newRoute1.getFitness());
//        System.out.println(newRoute2.getFitness());
//        System.out.println(newRoute3.getFitness());
//
//        conf.getRetailers().get(0).setOrders(newRoute1.route);
//        conf.getRetailers().get(1).setOrders(newRoute2.route);
//        conf.getRetailers().get(2).setOrders(newRoute3.route);
//
//        conf.getRetailers().get(0).setRoute(conf.getRetailers().get(0).getOrders());
//        conf.getRetailers().get(1).setRoute(conf.getRetailers().get(1).getOrders());
//        conf.getRetailers().get(2).setRoute(conf.getRetailers().get(2).getOrders());


        Utils.writeFileToConfig(conf, "./scripts/assginedOrders.csv");
        System.out.println(conf);

    }
}
