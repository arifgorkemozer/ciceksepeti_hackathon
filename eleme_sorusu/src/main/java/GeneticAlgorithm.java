
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author kulah
 */
public class GeneticAlgorithm {

    private List<Configuration> population;
    private double bestFitness;
    private int maxIteration;

    public GeneticAlgorithm(List<Configuration> initialPopulation, double bestFit, int maxIteration) {
        this.population = initialPopulation;
        this.bestFitness = bestFit;
        this.maxIteration = maxIteration;
    }

    public double getMinFitness() {
        double minFitness = 9999999999.0;

        for (Configuration configuration : population) {
            if (configuration.getFitness() < minFitness) {
                minFitness = configuration.getFitness();
            }
        }
        return minFitness;
    }

    public Configuration getBestConf() {
        sortPopulation();
        return population.get(0);
    }

    public void doMutations() {
        Random rand = new Random();
        float prob;
        int confInd = rouletteWheelSelection();
        for (int i = 0; i < 100; i++) {
            prob = rand.nextFloat();
            if (prob < 0.1) {
                confInd = rand.nextInt(population.size());
                Configuration child = population.get(confInd).mutation();
//                System.exit(0);
                if (child != null) {
                    population.add(child);
                }
//                System.out.println("=== Mutation added to population.New size: " + population.size());
            }
        }
    }

    public void doCrossovers() {
        Random rand = new Random();
        float prob;
        int confInd1;
        int confInd2 = -1;
        for (int i = 0; i < 100; i++) {
            prob = rand.nextFloat();
            if (prob < 0.1) {
                confInd1 = rouletteWheelSelection();

                // continue selecting until new distinct index found
                while (confInd2 == -1 || confInd2 == confInd1) {
                    confInd2 = rouletteWheelSelection();
                }

                Configuration child = population.get(confInd1).crossover(population.get(confInd2));

                population.add(child);
            }
        }
    }

    public void sortPopulation() {
        Collections.sort(population, new Comparator<Configuration>() {
            @Override
            public int compare(Configuration lhs, Configuration rhs) {
                // 1 - less than, -1 - greater than, 0 - equal, all inversed for descending
                return lhs.getFitness() > rhs.getFitness() ? 1 : (lhs.getFitness() < rhs.getFitness()) ? -1 : 0;
            }
        });
    }

    public int rouletteWheelSelection() {
        Random rand = new Random();
        float random = rand.nextFloat();
        if (random < 0.6) {
            return rand.nextInt(population.size() / 10);
        } else if (random < 0.8) {
            return rand.nextInt(population.size() / 3);
        } else {
            return rand.nextInt(population.size());
        }
    }

    public void naturalSelection() {
        Random rand = new Random();
        sortPopulation();
        
        /*
        Select 3 from top %10 of population. Kill them by probability of %20.
        */
        for (int i = 0; i < 7; i++) {
            if (population.size() > Main.POPULATION_SIZE) {
                if (rand.nextFloat() < 0.1) {
                    int ind = rand.nextInt(40);
                    population.remove(ind);
                }
            }
        }
        
        
        /*
        Select n from least m individual. Kill them by probability of %80.
        n = current population size - main population size
        m = n + 10
        */
        int range = 0;
        int ind = 0;
        while (population.size() > Main.POPULATION_SIZE) {
            if (rand.nextFloat() < 0.8) {
                range = population.size() - (Main.POPULATION_SIZE - 160);
                ind = (Main.POPULATION_SIZE - 160) + rand.nextInt(range);

                population.remove(ind);
            }
        }
        sortPopulation();
        System.out.println("-------------------------------------------");
    }

    private void getTopFitnessScores() {
        sortPopulation();
        for (int i = 0; i < 3; i++) {
            System.out.println("\tTop - " + i + ": " + population.get(i).getFitness() + "\n");
        }
    }

    public Configuration runAlgorithm() {
        int iterationNumber = 0;
        while (getMinFitness() > bestFitness) {
            sortPopulation();

            doMutations();
            doCrossovers();
            iterationNumber++;
            if (iterationNumber == maxIteration) {
                System.out.println("MAX ITERATION COUNT REACHED");
                return getBestConf();
            }
            naturalSelection();

            System.out.println("Iteration Count: " + iterationNumber + "\n");
            getTopFitnessScores();
            System.out.println("Population size : " + population.size());
        }
        return getBestConf();
    }

}
