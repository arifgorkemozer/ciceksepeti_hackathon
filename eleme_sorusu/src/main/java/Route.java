import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Route {
    public Retailer retailer;
    public List<Order> route;

    public Route(Retailer r, Collection<Order> orders) {
        this.retailer = new Retailer(r);
        this.retailer.clearOrders();
        this.route = new ArrayList<>();

        for (Order o : orders) {
            this.route.add(o);
        }
    }

    public double getFitness() {
        // firstly, move retailer to first order
        double totalDistance = Utils.calculateDistance(this.retailer, this.route.get(0));

        // internal moves Order(n) to Order(n + 1)
        for (int i = 0; i < this.route.size() - 1; i++) {
            Order o1 = this.route.get(i);
            Order o2 = this.route.get(i + 1);
            totalDistance += Utils.calculateDistance(o1, o2);
        }

        // finally, move back (Last order to retailer)
        totalDistance += Utils.calculateDistance(this.retailer, this.route.get(route.size() - 1));
        return totalDistance;
    }
}
