import java.util.ArrayList;
import java.util.List;

public class TSPSolver {

    public static class TSPResult {
        public double totalDistance;
        public List<Order> route;
    }

    public static double[][] createAdjacencyMatrix(Retailer retailer) {

        int orderCount = retailer.getOrderCount();
        int nodeCount = orderCount + 1;
        Order orders[] = new Order[orderCount];
        retailer.getOrders().toArray(orders);

        double adjacencyMatrix[][] = new double[nodeCount][nodeCount];

        // Calculate distance from retailer to all nodes
        for (int i = 0; i < orderCount; i++) {
            adjacencyMatrix[0][i + 1] = Utils.calculateDistance(retailer, orders[i]);
            adjacencyMatrix[i + 1][0] = Utils.calculateDistance(retailer, orders[i]);
        }

        // Calculate distance between all nodes
        for (int i = 0; i < orderCount; i++) {
            for (int j = 0; j < orderCount; j++) {
                if (i != j) {
                    adjacencyMatrix[i + 1][j + 1] = Utils.calculateDistance(orders[i], orders[j]);
                    adjacencyMatrix[j + 1][i + 1] = Utils.calculateDistance(orders[i], orders[j]);
                }
            }
        }
        
        return adjacencyMatrix;
    }

    public static TSPResult nearestNeighbour(Retailer retailer) {

        int orderCount = retailer.getOrderCount();
        int nodeCount = orderCount + 1;
        Order orders[] = new Order[orderCount];
        retailer.getOrders().toArray(orders);

        double adjacencyMatrix[][] = createAdjacencyMatrix(retailer);
        boolean isVisited[] = new boolean[nodeCount];
        int route[] = new int[nodeCount];
        double totalDistance = 0;

        int currentNode = 0;
        int visitedCount = 1;
        isVisited[0] = true;
        while (visitedCount < nodeCount) {

            double minDis = Double.MAX_VALUE;
            int nextNode = -1;

            // find nearest node
            for (int i = 0; i < nodeCount; i++) {
                if (!isVisited[i] && adjacencyMatrix[currentNode][i] < minDis) {
                    minDis = adjacencyMatrix[currentNode][i];
                    nextNode = i;
                }
            }

            // visit nearest node
            totalDistance += minDis;
            currentNode = nextNode;
            isVisited[nextNode] = true;
            route[visitedCount] = nextNode;

            visitedCount += 1;
        }

        List<Order> routesAsList = new ArrayList();
        for (int i = 0; i < orderCount; i++) {
            routesAsList.add(orders[route[i + 1] - 1]);
        }

        TSPResult result = new TSPResult();
        result.totalDistance = totalDistance;
        result.route = routesAsList;

        return result;
    }

    public static TSPResult simulateAnnealing(Retailer retailer) {
        double startingTemperature = Main.TSP_STARTING_TEMP;
        int numberOfIterations = Main.TSP_ITERATION_COUNT;
        double coolingRate = Main.TSP_COOLING_RATE;

        //System.out.println("Starting SA with temperature: " + startingTemperature + ", # of iterations: " + numberOfIterations + " and colling rate: " + coolingRate);
        long startTime = System.currentTimeMillis();

        double t = startingTemperature;
        Travel travel = new Travel(retailer);

        double bestDistance = travel.getTotalRouteDistance();
        double prevBestDistance = travel.getTotalRouteDistance();

        //System.out.println("Initial distance of travel: " + bestDistance);
        Travel bestSolution = new Travel(travel);
        Travel currentSolution = new Travel(travel);

        for (int i = 0; i < numberOfIterations; i++) {
            currentSolution.swapOrders();
            double currentDistance = currentSolution.getTotalRouteDistance();

            if (currentDistance < bestDistance) {
                bestSolution = new Travel(currentSolution);
                bestDistance = currentDistance;
            } else if (Math.exp((bestDistance - currentDistance) / t) < Math.random()) {
                currentSolution.revertSwap();
            }
            if (t > 0.01) {
                t *= coolingRate;
            }

            if (i % 100000 == 0) {
                //System.out.println("Iteration #" + i + " bestDistance: " + bestDistance + " - Temprature: " + t);
            }
            if (i % 100000 == 0) {
                double diff = Math.abs(prevBestDistance - bestDistance);
                if (diff < 0.00001) {
                    // return bestDistance;
                }
                //System.out.println("diff: " + diff);
                prevBestDistance = bestDistance;
            }
        }
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        // System.out.println("Iteration count: " + numberOfIterations + " - elapsedTime: " + elapsedTime);

        // bestSolution.printRoute();
        TSPResult result = new TSPResult();
        result.totalDistance = bestDistance;
        result.route = Utils.copyOrderList(bestSolution.getRoute());
        return result;
    }

}
