import java.util.Objects;

/**
 * Assignment between retailer and order.
 */
public class DistPair {
    private Retailer retailer;
    private Order order;

    public DistPair() {
    }

    public DistPair(Retailer retailer, Order order) {
        this.retailer = retailer;
        this.order = order;
    }

    public Retailer getRetailer() {
        return retailer;
    }

    public void setRetailer(Retailer retailer) {
        this.retailer = retailer;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DistPair that = (DistPair) o;
        return Objects.equals(retailer, that.retailer) &&
                Objects.equals(order, that.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(retailer, order);
    }
}
