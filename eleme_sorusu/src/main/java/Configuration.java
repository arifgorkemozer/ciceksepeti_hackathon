import java.util.*;

/**
 * Contains distribution pairs.
 */
public class Configuration implements Comparator<Configuration> {
    private List<Retailer> retailers;
    private double fitness = -1;

    public List<Retailer> getRetailers() {
        return retailers;
    }

    /*
        This constructor copies retailers. It will be used to create an empty
        configuration. It clears orders of reatilers.
        */
    public Configuration(List<Retailer> retailers) {
        this.retailers = new ArrayList();
        for (Retailer retailer : retailers) {
            Retailer tmpRetailer = new Retailer(retailer);
            tmpRetailer.clearOrders();

            this.retailers.add(tmpRetailer);
        }
    }

    /*
    This is copy constructor. It copies all reatilers with its own orders.
    It is used during genetic algorithm.
    */
    public Configuration(Configuration config) {
        this.retailers = new ArrayList();
        for (Retailer retailer : config.getRetailers()) {
            this.retailers.add(new Retailer(retailer));
        }
    }

    /**
     * @param allRetailers
     * @param allOrders
     * @return New random configuration.
     */
    public static Configuration generateRandom(List<Retailer> allRetailers, List<Order> allOrders) {
        Random rand = new Random();
        Configuration conf = new Configuration(allRetailers);

        List<Order> shrinkingOrderList = new ArrayList<>();
        shrinkingOrderList.addAll(allOrders);

        int currRetailerIndex = 0;

        // fill min quotas first
        while (shrinkingOrderList.size() > 0 && currRetailerIndex < conf.getRetailers().size()) {
            int randOrderIndex = rand.nextInt(shrinkingOrderList.size());
            Order o = shrinkingOrderList.get(randOrderIndex);

            // try to add order to current retailer
            // if fails, continue with next retailer
            if (conf.getRetailers().get(currRetailerIndex).getOrderCount() < conf.getRetailers().get(currRetailerIndex).getMinQuota()) {
                conf.getRetailers().get(currRetailerIndex).addOrder(o);
                shrinkingOrderList.remove(randOrderIndex);
            } else {
                currRetailerIndex++;
            }
        }

        // assign remaining orders randomly
        while (shrinkingOrderList.size() > 0) {

            int randRetailerIndex = rand.nextInt(conf.getRetailers().size());
            int randOrderIndex = rand.nextInt(shrinkingOrderList.size());
            Order o = shrinkingOrderList.get(randOrderIndex);

            // if random retailer is valid, add order
            if (conf.getRetailers().get(randRetailerIndex).canAddOrder()) {
                conf.getRetailers().get(randRetailerIndex).addOrder(o);
                shrinkingOrderList.remove(randOrderIndex);
            }
        }

        return conf;
    }

    /**
     * @param allOrders
     * @param setInitialCentroids
     * @return New configuration generated by K-means algorithm code.
     */
    public static Configuration generateWithKmeans(List<Order> allOrders, boolean setInitialCentroids) {
        KMeans KM = new KMeans(allOrders, setInitialCentroids);
        KM.clustering(3, 10, null); // 3 clusters, maximum 10 iterations
        return KM.getConfiguration();
    }

    public boolean isValid() {
        int totalOrderCount = 0;

        if (this.retailers == null)
            return false;

        for (Retailer retailer : retailers) {
            if (!retailer.isValid())
                return false;
            totalOrderCount += retailer.getOrderCount();
        }
        if (totalOrderCount != Main.ALL_ORDERS.size())
            return false;
        return true;
    }


    @Override
    public String toString() {
        String confStr = "Configuration {\n";

        for (int i = 0; i < this.retailers.size(); i++) {

            confStr += "\t";
            confStr += this.retailers.get(i).getName();
            confStr += " (" + Integer.toString(this.retailers.get(i).getOrderCount())
                    + " orders, quota is ["
                    + this.retailers.get(i).getMinQuota() + "-" + this.retailers.get(i).getMaxQuota()
                    + "], retailer location is (" + this.retailers.get(i).getLat() + " - " + this.retailers.get(i).getLng() + ")"
                    + "):\n";

            confStr += "\t\tOrders (Siparisler):\t ";

            List<Order> retailerOrders = new ArrayList<>();
            retailerOrders.addAll(this.retailers.get(i).getOrders());

            for (int j = 0; j < retailerOrders.size(); j++) {
                confStr += (retailerOrders.get(j).getOrderNumber() + ",");
            }

            confStr = confStr.substring(0, confStr.length() - 1);
            confStr += "\n\n";

            confStr += "\t\tRoute (Rota):\t";

            List<Order> retailerRoute = new ArrayList<>();
            retailerRoute.addAll(this.retailers.get(i).getRoute());

            for (int j = 0; j < retailerRoute.size(); j++) {
                confStr += (retailerRoute.get(j).getOrderNumber() + ",");
            }

            confStr = confStr.substring(0, confStr.length() - 1);
            confStr += "\n\n";
        }

        confStr += "}\n";

        return confStr;
    }

    public Configuration mutation() {
        Configuration child = new Configuration(this);
        Random rand = new Random();
        int retailerSize = child.getRetailers().size();
        int retailerToGetOrder = rand.nextInt(retailerSize);
        int retailerToPutOrder;
        boolean canMutate = false;

        int safetyCounter = 0;      // to escape infinite loop
        while (!child.getRetailers().get(retailerToGetOrder).canRemoveOrder()) {
            retailerToGetOrder = (retailerToGetOrder + 1) % retailerSize;

            safetyCounter += 1;
            // We cannot romove any order from any retailer
            if (safetyCounter > retailerSize)
                return null;
        }

        for (int i = 0; i < retailerSize; i++) {
            if (i != retailerToGetOrder && child.getRetailers().get(i).canAddOrder()) {
                canMutate = true;
            }
        }
        if (!canMutate) {
            return null;
        }

        safetyCounter = 0;      // to escape infinite loop
        retailerToPutOrder = rand.nextInt(retailerSize);
        while (retailerToPutOrder == retailerToGetOrder ||
                !child.getRetailers().get(retailerToPutOrder).canAddOrder()) {
            retailerToPutOrder = (retailerToPutOrder + 1) % retailerSize;

            safetyCounter += 1;
            if (safetyCounter > retailerSize)
                return null;
        }

        int orderToGet = rand.nextInt(child.getRetailers().get(retailerToGetOrder).getOrderCount());
        Order[] orders = child.getRetailers().get(retailerToGetOrder).getOrders().toArray(
                new Order[child.getRetailers().get(retailerToGetOrder).getOrderCount()]);
        Order order = orders[orderToGet];
        child.getRetailers().get(retailerToGetOrder).getOrders().remove(order);
        List<Order> clone = Utils.copyOrderList(child.getRetailers().get(retailerToPutOrder).getOrders());
        int orderIndexToPut = rand.nextInt(child.getRetailers().get(retailerToPutOrder).getOrders().size());
        clone.add(orderIndexToPut, order);
        child.getRetailers().get(retailerToPutOrder).setOrders(clone);

        return child;
    }

    public double getFitness() {
        if (fitness < 0) {
            double score = 0;

            for (Retailer retailer : retailers) {
                // TSPSolver.TSPResult result = TSPSolver.simulateAnnealing(retailer);
                TSPSolver.TSPResult result = TSPSolver.nearestNeighbour(retailer);
                score += result.totalDistance;
                retailer.setRoute(result.route);
            }
            fitness = score;
        }
        return fitness;
    }

    public double getFitnessGenetic() {
        if (fitness < 0) {
            double score = 0;
            for (Retailer retailer : retailers) {
                RouteFinder routeFinder = new RouteFinder(retailer, 50, 100);
                Route newRoute = routeFinder.run();
                score += newRoute.getFitness();
            }
            fitness = score;
        }
        return fitness;
    }

    public double eucledianScore() {
        double score = 0;
        for (Retailer retailer : retailers) {
            for (Order order : retailer.getOrders()) {
                score += Utils.calculateDistance(retailer, order);
            }
        }
        return score;
    }

    @Override
    public int compare(Configuration o1, Configuration o2) {
        if (o1.getFitness() > o2.getFitness())
            return 1;
        else if (o1.getFitness() == o2.getFitness())
            return 0;
        else
            return -1;
    }

    /**
     * @param otherConf
     * @return New configuration, mix of this configuration and given configuration.
     */
    public Configuration crossover(Configuration otherConf) {

        Random rand = new Random();
        Configuration crossoverResult = null;

        while (true) {
            crossoverResult = new Configuration(this);

            // select retailer a from this conf
            // select retailer b from other conf
            int randIndexRetailerA = rand.nextInt(crossoverResult.getRetailers().size());
            int randIndexRetailerB = rand.nextInt(otherConf.getRetailers().size());
            Retailer retailerA = crossoverResult.getRetailers().get(randIndexRetailerA);
            Retailer retailerB = otherConf.getRetailers().get(randIndexRetailerB);

            List<Order> ordersA = retailerA.getOrders();
            List<Order> ordersB = retailerB.getOrders();

            // find retailer b orders that are above minQuota
            int aboveMinQuotaCount = ordersB.size() - retailerB.getMinQuota();

            // don't try to do crossover on valid retailers
            if (aboveMinQuotaCount <= 0) {
                continue;
            } else {
                List<Order> selected_ordersB_diff_ordersA = new ArrayList<>();
                selected_ordersB_diff_ordersA.addAll(ordersB);
                selected_ordersB_diff_ordersA = selected_ordersB_diff_ordersA.subList(retailerB.getMinQuota(), ordersB.size());

                // find difference between "selected retailer b orders" and "retailer a orders"
                selected_ordersB_diff_ordersA.removeAll(ordersA);

                // if no different orders found, try again
                if (selected_ordersB_diff_ordersA.isEmpty()) {
                    continue;
                } else {
                    // select same number of "retailer a orders" with "selected_ordersB_diff_ordersA"
                    int replacedCount = selected_ordersB_diff_ordersA.size();

                    if (replacedCount > ordersA.size()) {
                        replacedCount = ordersA.size() / 2;

                        selected_ordersB_diff_ordersA = selected_ordersB_diff_ordersA.subList(0, replacedCount);
                    }

                    List<Order> selected_ordersA = new ArrayList<>();
                    selected_ordersA.addAll(ordersA);
                    selected_ordersA = selected_ordersA.subList(0, replacedCount);

                    // remove "selected_ordersA" from "retailer a orders"
                    crossoverResult.getRetailers().get(randIndexRetailerA).removeMultipleOrders(selected_ordersA);

                    // add "selected_ordersB_diff_ordersA" to "retailer a orders"
                    crossoverResult.getRetailers().get(randIndexRetailerA).addMultipleOrdersToIndex(0, selected_ordersB_diff_ordersA);

                    // remove "selected_ordersB_diff_ordersA" from "retailers of this conf", except "retailer a"
                    for (int i = 0; i < crossoverResult.getRetailers().size(); i++) {

                        if (!crossoverResult.getRetailers().get(i).getName().equals(retailerA.getName())) {
                            int deletedOrderCount = crossoverResult.getRetailers().get(i).removeMultipleOrders(selected_ordersB_diff_ordersA);

                            if (deletedOrderCount > 0) {

                                List<Order> replaceForOtherRetailer = new ArrayList<>();
                                replaceForOtherRetailer.addAll(selected_ordersA.subList(0, deletedOrderCount));
                                crossoverResult.getRetailers().get(i).addMultipleOrdersToIndex(0, replaceForOtherRetailer);

                                selected_ordersA.removeAll(replaceForOtherRetailer);

                                if (selected_ordersA.isEmpty()) {
                                    break;
                                }
                            }
                        }
                    }

                    break;
                }
            }
        }

        return crossoverResult;
    }

}