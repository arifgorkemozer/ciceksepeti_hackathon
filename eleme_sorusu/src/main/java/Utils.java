
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author semih
 */
public class Utils {

    /* DISTANCE UTILS */
    public static double calculateDistance(double lat1, double lon1, double lat2,
                                           double lon2) {

//        final int R = 6371; // Radius of the earth
//
//        double latDistance = Math.toRadians(lat2 - lat1);
//        double lonDistance = Math.toRadians(lon2 - lon1);
//        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
//                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
//                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
//        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
//        return R * c; // convert to meters
        return Math.sqrt(Math.pow(lat1 - lat2, 2) + Math.pow(lon1 - lon2, 2));
    }

    public static double calculateDistance(Order ord1, Order ord2) {
        double x1 = ord1.getLat();
        double y1 = ord1.getLng();

        double x2 = ord2.getLat();
        double y2 = ord2.getLng();

        return calculateDistance(x1, y1, x2, y2);
    }

    public static double calculateDistance(Retailer retailer, Order ord) {
        double x1 = retailer.getLat();
        double y1 = retailer.getLng();

        double x2 = ord.getLat();
        double y2 = ord.getLng();

        return calculateDistance(x1, y1, x2, y2);
    }

    /* EXCEL UTILS */
    public static List<Position> read_sheet(XSSFSheet sheet) {
        List<Position> pos = new ArrayList();
        Iterator<Row> rowIterator = sheet.iterator();

        // skip first line it is header
        rowIterator.next();

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();

            Cell cell = cellIterator.next();
            String id = cell.getStringCellValue();

            cell = cellIterator.next();
            double latitude = Double.parseDouble(cell.getStringCellValue().replace(",", "."));

            cell = cellIterator.next();
            double longitude = Double.parseDouble(cell.getStringCellValue().replace(",", "."));

            pos.add(new Position(id, latitude, longitude));
        }
        return pos;
    }

    public static XSSFWorkbook read_xlsx(String path) throws Exception {
        File excelFile = new File(path);
        FileInputStream fis = new FileInputStream(excelFile);

        return new XSSFWorkbook(fis);
    }

    public static List<Order> copyOrderList(Collection<Order> orders) {
        List<Order> copyOrders = new ArrayList();
        for (Order order : orders) {
            copyOrders.add(order);
        }

        return copyOrders;
    }

    public static void scalePositions(List<Position> posList1, List<Position> posList2, double minMaxValues[], double scaleFactor) {
        double minLng = minMaxValues[0];
        double maxLng = minMaxValues[1];
        double minLat = minMaxValues[2];
        double maxLat = minMaxValues[3];

        for (int i = 0; i < posList1.size(); i++) {
            Position pos = posList1.get(i);
            double lat = pos.getLatitude();
            double lng = pos.getLongitude();

            pos.setLatitude(scaleFactor * ((lat - minLat) / (maxLat - minLat)));
            pos.setLongitude(scaleFactor * ((lng - minLng) / (maxLng - minLng)));
        }

        for (int i = 0; i < posList2.size(); i++) {
            Position pos = posList2.get(i);
            double lat = pos.getLatitude();
            double lng = pos.getLongitude();

            pos.setLatitude(scaleFactor * ((lat - minLat) / (maxLat - minLat)));
            pos.setLongitude(scaleFactor * ((lng - minLng) / (maxLng - minLng)));
        }
    }

    public static double[] getMinMax(List<Position> posList1, List<Position> posList2) {
        double minLng = 9999, maxLng = 0;
        double minLat = 9999, maxLat = 0;

        for (int i = 0; i < posList1.size(); i++) {
            Position pos = posList1.get(i);
            double lat = pos.getLatitude();
            double lng = pos.getLongitude();
            if (lat < minLat) {
                minLat = lat;
            }
            if (lat > maxLat) {
                maxLat = lat;
            }

            if (lng < minLng) {
                minLng = lng;
            }
            if (lng > maxLng) {
                maxLng = lng;
            }
        }

        for (int i = 0; i < posList2.size(); i++) {
            Position pos = posList2.get(i);
            double lat = pos.getLatitude();
            double lng = pos.getLongitude();
            if (lat < minLat) {
                minLat = lat;
            }
            if (lat > maxLat) {
                maxLat = lat;
            }

            if (lng < minLng) {
                minLng = lng;
            }
            if (lng > maxLng) {
                maxLng = lng;
            }
        }

        return new double[]{minLng, maxLng, minLat, maxLat};
    }

    public static void writeFileToConfig(Configuration conf, String filePath) {
        OutputStream os = null;
        String data = "";
        for (int i = 0; i < conf.getRetailers().size(); i++) {
            String tmpData = "";
            Retailer retailer = conf.getRetailers().get(i);
            List<Order> route = retailer.getRoute();
            for (int j = 0; j < route.size(); j++) {
                Order order = route.get(j);
                tmpData += order.getOrderNumber();
                if (j != route.size() - 1)
                    tmpData += ",";
            }
            data += tmpData;

            if (i != conf.getRetailers().size() - 1)
                data += "\n";
        }

        try {
            os = new FileOutputStream(new File(filePath));
            os.write(data.getBytes(), 0, data.length());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
