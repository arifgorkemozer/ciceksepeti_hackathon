
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author semih
 */
public class Travel {
    private List<Order> route = new ArrayList<>();
    private Retailer retailer;
    private int last_swap[] = new int[2];

    public Travel(Travel travel) {
        this.retailer = travel.retailer;
        this.route = Utils.copyOrderList(travel.getRoute());
    }

    public List<Order> getRoute() {
        return this.route;
    }
    
    public Travel(Retailer retailer) {
        this.retailer = retailer;
        int i = 0;
        int maxCount = 20;

        for (Order order : retailer.getOrders()) {
            route.add(order);
            i += 1;
            if (i >= maxCount)
                break;
        }
    }

    public void swapOrders() {
        Random rand = new Random();
        int id1 = rand.nextInt(this.route.size());
        int id2 = -1;

        while (id2 == -1 || id2 == id1) {
            id2 = rand.nextInt(this.route.size());
        }

        Order o1 = this.route.get(id1);
        Order o2 = this.route.get(id2);
        this.route.set(id1, o2);
        this.route.set(id2, o1);

        this.last_swap[0] = id1;
        this.last_swap[1] = id2;
    }

    public void revertSwap() {
        int id1 = this.last_swap[1];
        int id2 = this.last_swap[0];

        Order o1 = this.route.get(id1);
        Order o2 = this.route.get(id2);
        this.route.set(id1, o2);
        this.route.set(id2, o1);
    }

    public Order getOrder(int index) {
        return this.route.get(index);
    }

    public double getTotalRouteDistance() {
        // firstly, move retailer to first order
        double totalDistance = Utils.calculateDistance(this.retailer, getOrder(0));

        // internal moves Order(n) to Order(n + 1)
        for (int index = 0; index < this.route.size() - 1; index++) {
            Order o1 = getOrder(index);
            Order o2 = getOrder(index + 1);
            totalDistance += Utils.calculateDistance(o1, o2);
        }

        // finally, move back (Last order to retailer)
        totalDistance += Utils.calculateDistance(this.retailer, getOrder(route.size() - 1));
        return totalDistance;
    }

    public void printRoute() {
        // print retailer
        System.out.println(retailer.getLat() + " " + retailer.getLng());
        for (Order order : route) {
            System.out.println(order.getLat() + " " + order.getLng());
        }
        System.out.println(retailer.getLat() + " " + retailer.getLng());
    }

    public void printRoute(List<Order> route) {
        for (int i = 0; i < route.size(); i++) {
            Order order = route.get(i);
            System.out.println(i + " Order_" + order.getOrderNumber() + " - "
                    + order.getLat() + " " + order.getLng());
        }
    }
}
