import matplotlib.pyplot as plt
from math import sin, cos, sqrt, atan2, radians
from pprint import pprint

def euc(la1, la2, lo1, lo2):
	# approximate radius of earth in km
	R = 6373.0

	lat1 = radians(la1)
	lon1 = radians(lo1)
	lat2 = radians(la2)
	lon2 = radians(lo2)

	dlon = lon2 - lon1
	dlat = lat2 - lat1

	a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
	c = 2 * atan2(sqrt(a), sqrt(1 - a))

	distance = R * c
	return distance

def readRetailers():
	retailers = []
	with open("retailers.csv", "r") as file:
		lines = file.readlines()

	for line in lines:
		arr = line.split()
		retailers.append((arr[0], float(arr[1]), float(arr[2])))

	return retailers

def readOrders():
	ordersDict = {}

	with open("orders.csv", "r") as file:
		lines = file.readlines()

	for line in lines:
		arr = line.split()
		ordersDict[arr[0]] = (float(arr[1]), float(arr[2]))

	return ordersDict

def readAssignedOrders():
	allOrderIds = []
	assignedOrders = []
	with open("assginedOrders.csv", "r") as file:
		lines = file.readlines()

	for line in lines:
		arr = line.strip().split(',')
		arr = list(filter(lambda x: x, arr))
		allOrderIds += [int(x) for x in arr]
		assignedOrders.append(arr)

	allOrderIds.sort()
	for i in range(100, 200):
		if allOrderIds[i - 100] != i:
			pprint(allOrderIds)
			raise Exception("Order id error!")

	return assignedOrders

def drawRoute(retailer, ordersDict, retailer_orders):
	orders_route = [(ordersDict[no][0], ordersDict[no][1], no) for no in retailer_orders]
	retailer_point = (retailer[1], retailer[2], 'R')
	retailer_route = [retailer_point] + orders_route + [retailer_point]

	data_x = [x[0] for x in retailer_route]
	data_y = [x[1] for x in retailer_route]

	dists = []
	for c, x in enumerate(retailer_route[:-1]):
		prev = 0 if not dists else dists[-1]
		_euc = euc(x[0], retailer_route[c+1][0], x[1], retailer_route[c+1][1])
		print ('%.4f-%.4f (%-3s)   %.4f-%.4f (%3s) ===> %.4f' % (x[0], x[1], x[2], retailer_route[c+1][0], retailer_route[c+1][1], retailer_route[c+1][2], _euc))
		dists.append(prev + _euc)

	fig, axs = plt.subplots(2, 1)
	axs[0].plot(data_x[0], data_y[0], 'ro')
	axs[0].plot(data_x, data_y)
	axs[1].set_yticks(range(0, 30, 2))
	axs[1].plot(range(len(dists)), dists)
	plt.show()

ordersDict = readOrders()
retailers = readRetailers()
assignedOrders = readAssignedOrders()

colors = ["r", "b", "g"]
for idx, orders in enumerate(assignedOrders):
	x_data = [ordersDict[no][0] for no in orders]
	y_data = [ordersDict[no][1] for no in orders]
	plt.scatter(x_data, y_data, color=colors[idx])

	x_data = [retailers[idx][1]] + x_data + [retailers[idx][1]]
	y_data = [retailers[idx][2]] + y_data + [retailers[idx][2]]

	plt.plot(x_data, y_data, color=colors[idx])

colors = ['y', 'k', 'm']
for idx, retailer in enumerate(retailers):
	plt.scatter([retailer[1]], [retailer[2]], color=colors[idx], s=100)
plt.show()


#for i in range(len(retailers)):
#	drawRoute(retailers[i], ordersDict, assignedOrders[i])

