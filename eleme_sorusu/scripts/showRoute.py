import matplotlib.pyplot as plt
from math import sin, cos, sqrt, atan2, radians

def euc(la1, la2, lo1, lo2):
	# approximate radius of earth in km
	R = 6373.0

	lat1 = radians(la1)
	lon1 = radians(lo1)
	lat2 = radians(la2)
	lon2 = radians(lo2)

	dlon = lon2 - lon1
	dlat = lat2 - lat1

	a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
	c = 2 * atan2(sqrt(a), sqrt(1 - a))

	distance = R * c
	return distance

with open('route', 'r') as f:
	data = f.readlines()

data = [[float(x.split(' ')[0]), float(x.split(' ')[1])] for x in data]
data_x = [x[0] for x in data]
data_y = [x[1] for x in data]

dists = []
for c, x in enumerate(data[:-1]):
	prev = 0 if not dists else dists[-1]
	_euc = euc(x[0], data[c+1][0], x[1], data[c+1][1])
	print '%.4f-%.4f  %.4f-%.4f ===> %.4f' % (x[0], x[1], data[c+1][0], data[c+1][1],_euc)
	dists.append(prev + _euc)

fig, axs = plt.subplots(2, 1)
axs[0].plot(data_x[0], data_y[0], 'ro')
axs[0].plot(data_x, data_y)
axs[1].set_yticks(range(0, 30, 2))
axs[1].plot(range(len(dists)), dists)
plt.show()