from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token

from django.http import Http404
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth import hashers
from django.utils import crypto
from django.utils import timezone
from django.contrib.auth import authenticate
from floudapi.serializers import *

from floudapi.models import *
from django.db.models import Q
from django.db.models import Sum

class SepetEkle(APIView):

    def post(self, request):
        alan_bayi_id = request.data.get('alan_bayi_id')
        teslimat_bayi_id = request.data.get('teslimat_bayi_id')
        musteri_id = request.data.get('musteri_id')
        tekil_kod = request.data.get('tekil_kod')
        durum = request.data.get('durum')
        note = request.data.get('note')
        siparis_tarihi = request.data.get('siparis_tarihi')
        teslimat_tarihi = request.data.get('teslimat_tarihi')
        teslimat_adresi_id = request.data.get('teslimat_adresi_id')
        hedef_musteri_id = request.data.get('hedef_musteri_id')

        serializer = SepetSerializer(data=request.data)

        if not serializer.is_valid():
            response_data = {
                'detail': 'sepet eklenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

        sepet = Sepet()
        sepet.alan_bayi_id = Bayi.objects.get(id=alan_bayi_id)
        sepet.teslimat_bayi_id = Bayi.objects.get(id=teslimat_bayi_id)
        sepet.musteri_id = Musteri.objects.get(id=musteri_id)
        sepet.tekil_kod = tekil_kod
        sepet.durum = durum
        sepet.note = note
        sepet.siparis_tarihi = siparis_tarihi
        sepet.teslimat_tarihi = teslimat_tarihi
        sepet.teslimat_adresi_id = Adres.objects.get(id=teslimat_adresi_id)
        sepet.hedef_musteri_id = Musteri.objects.get(id=hedef_musteri_id)
        sepet.save()
        response_data = {
            'detail': 'Sepet eklendi'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)


class SepetListeleBayi(APIView):

    def get(self, request, pk):
        sepetler = Sepet.objects.filter(Q(alan_bayi_id=pk) | Q(teslimat_bayi_id=pk)).order_by('-siparis_tarihi')


        serializer = SepetSerializer(sepetler, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class SepetListele(APIView):

    def get(self, request):
        sepetler = Sepet.objects.order_by('-teslimat_tarihi')

        serializer = SepetSerializer(sepetler, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class SepetDetay(APIView):

    def get_object(self, pk):
        try:
            return Sepet.objects.get(pk=pk)
        except Sepet.DoesNotExist:
            raise Http404

    def get(self, request, pk):

        sepet = self.get_object(pk)
        serializer = SepetSerializerTek(sepet)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):

        sepet = self.get_object(pk)

        serializer = SepetSerializer(sepet, data=request.data)

        sepet_onceki_durum = sepet.durum
        yeni_durum = request.data.get("durum")
        if serializer.is_valid():
            serializer.save()

            if sepet_onceki_durum == "H" and yeni_durum == "K":
                tutar = sepet.sepetkalemi_set.aggregate(Sum("tutar"))["tutar__sum"]

                cicek_sepeti_yuzdesi = 0.02
                alan_bayi_borc = tutar / 2 - tutar * cicek_sepeti_yuzdesi
                teslimat_bayi_alacak = tutar / 2 + tutar * cicek_sepeti_yuzdesi

                alan_bayi = sepet.alan_bayi_id
                teslimat_bayi = sepet.teslimat_bayi_id

                alan_bayi.bakiye -= alan_bayi_borc
                teslimat_bayi.bakiye += teslimat_bayi_alacak
                alan_bayi.save()
                teslimat_bayi.save()

                sepet.save()


            response_data = {
                'detail': 'sepet guncellendi'
            }
            return Response(data=response_data, status=status.HTTP_200_OK)

        else:
            response_data = {
                'detail': 'sepet guncellenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):

        sepet = self.get_object(pk)

        sepet.delete()
        response_data = {
            'detail': 'sepet silindi.'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)
