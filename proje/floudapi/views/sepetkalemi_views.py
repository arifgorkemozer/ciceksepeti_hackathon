from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token

from django.http import Http404
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth import hashers
from django.utils import crypto
from django.utils import timezone
from django.contrib.auth import authenticate
from floudapi.serializers import *

from floudapi.models import *


class SepetKalemiEkle(APIView):

    def post(self, request):
        sepet_id = request.data.get('sepet_id')
        urun_id = request.data.get('urun_id')
        miktar = request.data.get('miktar')


        serializer = SepetKalemiSerializer(data=request.data)

        if not serializer.is_valid():
            response_data = {
                'detail': 'sepet_kalemi eklenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

        # olusturdugun useri bayi objesine bagla
        sepet_kalemi = SepetKalemi()
        sepet_kalemi.sepet_id = Sepet.objects.get(id=sepet_id)
        sepet_kalemi.urun_id = Urun.objects.get(id=urun_id)
        sepet_kalemi.miktar = miktar
        sepet_kalemi.tutar = sepet_kalemi.urun_id.fiyat * miktar

        # bayi kaydet
        sepet_kalemi.save()

        response_data = {
            'detail': 'sepet_kalemi eklendi'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)


class SepetKalemiListele(APIView):

    def get(self, request):
        sepet_kalemiler = SepetKalemi.objects.order_by('-id')

        serializer = SepetKalemiSerializer(sepet_kalemiler, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class SepetKalemiListeleSepet(APIView):

    def get(self, request, pk):
        sepet_kalemiler = SepetKalemi.objects.filter(sepet_id=pk).order_by('-id')

        data = []

        for s in sepet_kalemiler:
            elem = {
                'id': s.id,
                'urun_id': s.urun_id.id,
                'urun_adi': s.urun_id.urun_adi,
                'urun_fiyat': s.urun_id.fiyat,
                'miktar': s.miktar,
                'tutar': s.tutar
            }
            data.append(elem)

        return Response(data=data, status=status.HTTP_200_OK)


class SepetKalemiDetay(APIView):

    def get_object(self, pk):
        try:
            return SepetKalemi.objects.get(pk=pk)
        except SepetKalemi.DoesNotExist:
            raise Http404

    def get(self, request, pk):

        sepet_kalemi = self.get_object(pk)
        serializer = SepetKalemiSerializer(sepet_kalemi)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):

        sepet_kalemi = self.get_object(pk)

        serializer = SepetKalemiSerializer(sepet_kalemi, data=request.data)

        if serializer.is_valid():
            serializer.save()

            response_data = {
                'detail': 'sepet_kalemi guncellendi'
            }
            return Response(data=response_data, status=status.HTTP_200_OK)

        else:
            response_data = {
                'detail': 'sepet_kalemi guncellenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):

        sepet_kalemi = self.get_object(pk)

        sepet_kalemi.delete()
        response_data = {
            'detail': 'sepet_kalemi silindi.'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)
