from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token

from django.http import Http404
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth import hashers
from django.utils import crypto
from django.utils import timezone
from django.contrib.auth import authenticate
from floudapi.serializers import *

from floudapi.models import *


class AdresEkle(APIView):

    def post(self, request):
        sehir = request.data.get('sehir')
        ilce = request.data.get('ilce')
        acik_adres = request.data.get('acik_adres')

        enlem = request.data.get('enlem')
        boylam = request.data.get('boylam')

        serializer = AdresSerializer(data=request.data)

        if not serializer.is_valid():
            response_data = {
                'detail': 'adres eklenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

        # olusturdugun useri bayi objesine bagla
        adres = Adres()
        adres.sehir = sehir
        adres.ilce = ilce
        adres.acik_adres = acik_adres

        adres.enlem = enlem
        adres.boylam = boylam

        # bayi kaydet
        adres.save()

        response_data = {
            'detail': 'adres eklendi'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)


class AdresListele(APIView):

    def get(self, request):
        adresler = Adres.objects.order_by('-id')

        serializer = AdresSerializer(adresler, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class AdresDetay(APIView):

    def get_object(self, pk):
        try:
            return Adres.objects.get(pk=pk)
        except Adres.DoesNotExist:
            raise Http404

    def get(self, request, pk):

        adres = self.get_object(pk)
        serializer = AdresSerializer(adres)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):

        adres = self.get_object(pk)

        serializer = AdresSerializer(adres, data=request.data)

        if serializer.is_valid():
            serializer.save()

            response_data = {
                'detail': 'adres guncellendi'
            }
            return Response(data=response_data, status=status.HTTP_200_OK)

        else:
            response_data = {
                'detail': 'adres guncellenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):

        adres = self.get_object(pk)

        adres.delete()
        response_data = {
            'detail': 'adres silindi.'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)
