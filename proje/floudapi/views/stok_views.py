from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token

from django.http import Http404
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth import hashers
from django.utils import crypto
from django.utils import timezone
from django.contrib.auth import authenticate
from floudapi.serializers import *

from floudapi.models import *

class StokEkle(APIView):

    def post(self, request):
        bayi_id = request.data.get('bayi_id')
        urun_id = request.data.get('urun_id')
        miktar = request.data.get('miktar')

        serializer = StokSerializer(data=request.data)

        if not serializer.is_valid():
            response_data = {
                'detail': 'stok eklenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

        # olusturdugun useri bayi objesine bagla
        stok = Stok()
        stok.bayi_id = Bayi.objects.get(id=bayi_id)
        stok.urun_id = Urun.objects.get(id=urun_id)
        stok.miktar = miktar

        # bayi kaydet
        stok.save()

        response_data = {
            'detail': 'stok eklendi'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)

class StokListeleBayi(APIView):

    def get(self, request, pk):
        stoklar = Stok.objects.filter(bayi_id=pk).order_by('-id')

        serializer = StokSerializer(stoklar, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class StokListele(APIView):

    def get(self, request):
        stoklar = Stok.objects.order_by('-id')

        serializer = StokSerializer(stoklar, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class StokDetay(APIView):

    def get_object(self, pk):
        try:
            return Stok.objects.get(pk=pk)
        except Stok.DoesNotExist:
            raise Http404

    def get(self, request, pk):

        stok = self.get_object(pk)
        serializer = StokSerializer(stok)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):

        stok = self.get_object(pk)

        serializer = StokSerializer(stok, data=request.data)

        if serializer.is_valid():
            serializer.save()

            response_data = {
                'detail': 'stok guncellendi'
            }
            return Response(data=response_data, status=status.HTTP_200_OK)

        else:
            response_data = {
                'detail': 'stok guncellenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):

        stok = self.get_object(pk)

        stok.delete()
        response_data = {
            'detail': 'stok silindi.'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)
