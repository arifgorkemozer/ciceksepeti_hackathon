from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token

from django.http import Http404
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth import hashers
from django.utils import crypto
from django.utils import timezone
from django.contrib.auth import authenticate
from floudapi.serializers import *

from floudapi.models import *


class UrunEkle(APIView):

    def post(self, request):
        urun_adi = request.data.get('urun_adi')
        aciklama = request.data.get('aciklama')
        resim_url = request.data.get('resim_url')
        fiyat = request.data.get('fiyat')


        serializer = UrunSerializer(data=request.data)

        if not serializer.is_valid():
            response_data = {
                'detail': 'urun eklenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

        # once default django useri olustur
        urun = Urun()
        urun.urun_adi = urun_adi
        urun.aciklama = aciklama
        urun.resim_url = resim_url
        urun.fiyat = fiyat
        urun.save()

        response_data = {
            'detail': 'urun eklendi'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)


class UrunListele(APIView):

    def get(self, request):
        urunler = Urun.objects.order_by('-id')

        serializer = UrunSerializer(urunler, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class UrunDetay(APIView):

    def get_object(self, pk):
        try:
            return Urun.objects.get(pk=pk)
        except Urun.DoesNotExist:
            raise Http404

    def get(self, request, pk):

        urun = self.get_object(pk)
        serializer = UrunSerializer(urun)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):

        urun = self.get_object(pk)

        serializer = UrunSerializer(urun, data=request.data)

        if serializer.is_valid():
            serializer.save()

            response_data = {
                'detail': 'urun guncellendi'
            }
            return Response(data=response_data, status=status.HTTP_200_OK)

        else:
            response_data = {
                'detail': 'urun guncellenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):

        urun = self.get_object(pk)

        urun.delete()
        response_data = {
            'detail': 'urun silindi.'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)
