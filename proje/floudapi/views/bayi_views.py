from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token

from django.http import Http404
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth import hashers
from django.utils import crypto
from django.utils import timezone
from django.contrib.auth import authenticate
from floudapi.serializers import *

from floudapi.models import *
import pprint
import time
from time import gmtime, strftime
import datetime

from math import sin, cos, sqrt, atan2, radians

from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
import numpy as np
import operator


class BayiLogin(APIView):

    def post(self, request):

        username = request.data.get('username')
        password = request.data.get('password')

        print(username)
        print(password)

        if username is None or password is None:
            response_data = {
                'detail': 'Please provide both username and password.'
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(username=username, password=password)

        if not user:
            response_data = {
                'detail': 'Invalid username or password entered.'
            }

            return Response(data=response_data, status=status.HTTP_404_NOT_FOUND)

        user.last_login = timezone.now()
        user.save(update_fields=['last_login'])

        bayi = Bayi.objects.get(user=user)

        response_data = {
            'detail': 'Logged in successfully.',
            'bayi_id': bayi.id,
            'bayi_unvan': bayi.unvan,
            'bayi_aciklama': bayi.aciklama,
            'bayi_username': bayi.user.username,
            'bayi_adres_id': bayi.adres_id.id,
            'bayi_bakiye': bayi.bakiye
        }

        return Response(response_data, status=status.HTTP_200_OK)


class BayiEkle(APIView):

    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')
        email = request.data.get('email')
        first_name = request.data.get('first_name')
        last_name = request.data.get('last_name')

        sehir = request.data.get('sehir')
        ilce = request.data.get('ilce')
        acik_adres = request.data.get('acik_adres')
        enlem = request.data.get('enlem')
        boylam = request.data.get('boylam')

        unvan = request.data.get('unvan')
        aciklama = request.data.get('aciklama')
        bakiye = 0

        serializer = BayiEkleSerializer(data=request.data)

        if not serializer.is_valid():
            response_data = {
                'detail': 'bayi eklenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

        # once default django useri olustur
        user = User()
        user.username = username
        user.password = hashers.make_password(password=password,
                                              salt=crypto.get_random_string(10),
                                              hasher=hashers.PBKDF2PasswordHasher())
        user.email = email
        user.first_name = first_name
        user.last_name = last_name
        user.date_joined = timezone.now()
        user.save()

        adres = Adres()
        adres.sehir = sehir
        adres.ilce = ilce
        adres.acik_adres = acik_adres
        adres.enlem = enlem
        adres.boylam = boylam
        adres.save()

        # olusturdugun useri bayi objesine bagla
        bayi = Bayi()
        bayi.user = user
        bayi.unvan = unvan
        bayi.adres_id = adres
        bayi.aciklama = aciklama
        bayi.bakiye = bakiye

        # bayi kaydet
        bayi.save()

        response_data = {
            'detail': 'bayi eklendi'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)


class BayiListele(APIView):

    def get(self, request):
        bayiler = Bayi.objects.order_by('-id')

        serializer = BayiSerializer(bayiler, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class UygunBayiBulma(APIView):
    def get(self, request, pk):
        sepet = Sepet.objects.filter(id=pk)[0]
        sepet_kalemleri = SepetKalemi.objects.filter(sepet_id=pk)

        stoklar = Stok.objects.all()
        bayi_stoklari = {}
        for stok in stoklar:
            if not stok.bayi_id.id in bayi_stoklari:
                bayi_stoklari[stok.bayi_id.id] = {}
            bayi_stoklari[stok.bayi_id.id][stok.urun_id.id] = stok.miktar

        availables = []
        for bayi in bayi_stoklari:
            available = True
            for kalem in sepet_kalemleri:
                if kalem.urun_id.id in bayi_stoklari[bayi].keys() and kalem.miktar < bayi_stoklari[bayi][kalem.urun_id.id]:
                    continue
                available = False
            if available:
                availables.append(bayi)

        bayiler = Bayi.objects.filter(id__in=availables)

        hedef = (sepet.teslimat_adresi_id.enlem, sepet.teslimat_adresi_id.boylam)
        min_bayi = None
        min_menzil = 9999999999
        for bayi in bayiler:
            menzil = self.euc(hedef[0], bayi.adres_id.enlem, hedef[1], bayi.adres_id.boylam)

            if menzil < min_menzil:
                min_bayi = bayi
                min_menzil = menzil


        # serializer = BayiSerializer(min_bayi)
        # return Response(data=serializer.data, status=status.HTTP_200_OK)

        response_data = {
            'id': min_bayi.id,
            'bayi_username': min_bayi.user.username,
            'unvan': min_bayi.unvan,
            'aciklama': min_bayi.aciklama,
            'bakiye': min_bayi.bakiye,
            'enlem': min_bayi.adres_id.enlem,
            'boylam': min_bayi.adres_id.boylam
        }

        return Response(data=response_data, status=status.HTTP_200_OK)

    def euc(self, la1, la2, lo1, lo2):
        # approximate radius of earth in km
        R = 6373.0

        lat1 = radians(la1)
        lon1 = radians(lo1)
        lat2 = radians(la2)
        lon2 = radians(lo2)

        dlon = lon2 - lon1
        dlat = lat2 - lat1

        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        distance = R * c
        return distance

class BayiTavsiye(APIView):
    def euc(self, kalem_adres, bayi_adres):
        # approximate radius of earth in km
        R = 6373.0

        lat1 = radians(kalem_adres.enlem)
        lon1 = radians(kalem_adres.boylam)
        lat2 = radians(bayi_adres.enlem)
        lon2 = radians(bayi_adres.boylam)

        dlon = lon2 - lon1
        dlat = lat2 - lat1

        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        distance = R * c
        return distance
    
    def get(self, request, pk):
        bayi = Bayi.objects.filter(id=pk)[0]
        kalemler = SepetKalemi.objects.filter(sepet_id__teslimat_adresi_id__sehir=bayi.adres_id.sehir)
        print('len kalemler %d' % (len(kalemler)))
        bayiye_yakin_kalemler = list(filter(lambda kalem: self.euc(kalem.sepet_id.teslimat_adresi_id, bayi.adres_id) < 5, kalemler))
        weeks = []
        urun_ids = []
        for i in range(52):
            weeks.append({})

        for kalem in bayiye_yakin_kalemler:
            week = kalem.sepet_id.teslimat_tarihi.isocalendar()[1] % 52
            if not kalem.urun_id.id in urun_ids:
                urun_ids.append(kalem.urun_id.id)
            if not weeks[week].get(kalem.urun_id.id):
                weeks[week][kalem.urun_id.id] = 0
            weeks[week][kalem.urun_id.id] += kalem.miktar


        expecteds = []
        for urun in urun_ids:
            y = np.array([weeks[week].get(urun, 0) for week in range(52)]).reshape(-1, 1)
            x = np.array(range(52)).reshape(-1, 1)
            # plt.plot(range(52), data)
            # plt.show()
            polynomial_features = PolynomialFeatures(degree=5)
            x_poly = polynomial_features.fit_transform(x)

            model = linear_model.LinearRegression()
            model.fit(x_poly, y)
            y_poly_pred = model.predict(x_poly)
            expecteds.append({
                'urun_id': urun,
                'result': int(y_poly_pred[datetime.datetime.now().isocalendar()[1]])*3
            })
            # plt.scatter(x, y, s=30, label="Stok Satış Miktarı")
            #
            # plt.xlabel("Hafta")
            # plt.ylabel("Toplam Satış Miktarı")
            # plt.title("Stok Satış Miktarı")
            #
            # # # sort the values of x before line plot
            # sort_axis = operator.itemgetter(0)
            # sorted_zip = sorted(zip(x, y_poly_pred), key=sort_axis)
            # x, y_poly_pred = zip(*sorted_zip)
            # plt.plot(x, y_poly_pred, color='m')
            # plt.xticks(range(1, 53))
            # plt.legend()
            # plt.show()
        return Response(data=expecteds, status=status.HTTP_200_OK)

class BayiDetay(APIView):

    def get_object(self, pk):
        try:
            return Bayi.objects.get(pk=pk)
        except Bayi.DoesNotExist:
            raise Http404

    def get(self, request, pk):

        bayi = self.get_object(pk)
        serializer = BayiSerializer(bayi)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):

        bayi = self.get_object(pk)

        serializer = BayiSerializer(bayi, data=request.data)

        if serializer.is_valid():
            serializer.save()

            response_data = {
                'detail': 'bayi guncellendi'
            }
            return Response(data=response_data, status=status.HTTP_200_OK)

        else:
            response_data = {
                'detail': 'bayi guncellenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):

        bayi = self.get_object(pk)

        bayi.delete()
        response_data = {
            'detail': 'bayi silindi.'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)
