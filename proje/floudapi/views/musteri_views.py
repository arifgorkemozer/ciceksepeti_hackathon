from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token

from django.http import Http404
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth import hashers
from django.utils import crypto
from django.utils import timezone
from django.contrib.auth import authenticate
from floudapi.serializers import *

from floudapi.models import *


class MusteriEkle(APIView):

    def post(self, request):
        adres_id = request.data.get('adres_id')
        isim = request.data.get('isim')
        musteri_kodu = request.data.get('musteri_kodu')
        yas = request.data.get('yas')
        cinsiyet = request.data.get('cinsiyet')

        serializer = MusteriSerializer(data=request.data)

        if not serializer.is_valid():
            response_data = {
                'detail': 'musteri eklenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

        # olusturdugun useri bayi objesine bagla
        musteri = Musteri()
        musteri.adres_id = Adres.objects.get(id=adres_id)
        musteri.isim = isim
        musteri.musteri_kodu = musteri_kodu
        musteri.yas = yas
        musteri.cinsiyet = cinsiyet

        # bayi kaydet
        musteri.save()

        response_data = {
            'detail': 'musteri eklendi'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)


class MusteriListele(APIView):

    def get(self, request):
        musteriler = Musteri.objects.order_by('-id')

        serializer = MusteriSerializer(musteriler, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class MusteriDetay(APIView):

    def get_object(self, pk):
        try:
            return Musteri.objects.get(pk=pk)
        except Musteri.DoesNotExist:
            raise Http404

    def get(self, request, pk):

        musteri = self.get_object(pk)
        serializer = MusteriSerializer(musteri)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):

        musteri = self.get_object(pk)

        serializer = MusteriSerializer(musteri, data=request.data)

        if serializer.is_valid():
            serializer.save()

            response_data = {
                'detail': 'musteri guncellendi'
            }
            return Response(data=response_data, status=status.HTTP_200_OK)

        else:
            response_data = {
                'detail': 'musteri guncellenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):

        musteri = self.get_object(pk)

        musteri.delete()
        response_data = {
            'detail': 'musteri silindi.'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)
