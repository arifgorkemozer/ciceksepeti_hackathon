from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token

from django.http import Http404
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth import hashers
from django.utils import crypto
from django.utils import timezone
from django.contrib.auth import authenticate
from .serializers import *

from .models import *


class BayiLogin(APIView):

    def post(self, request):

        username = request.data.get('username')
        password = request.data.get('password')

        if username is None or password is None:
            response_data = {
                'detail': 'Please provide both username and password.'
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(username=username, password=password)

        if not user:
            response_data = {
                'detail': 'Invalid username or password entered.'
            }

            return Response(data=response_data, status=status.HTTP_404_NOT_FOUND)

        user.last_login = timezone.now()
        user.save(update_fields=['last_login'])

        bayi = Bayi.objects.get(user=user)

        response_data = {
            'detail': 'Logged in successfully.',
            'bayi_id': bayi.id,
            'bayi_username': bayi.user.username,
            'location': {
                'lat': bayi.location_lat,
                'lng': bayi.location_lng
            }
        }

        return Response(response_data, status=status.HTTP_200_OK)


class BayiEkle(APIView):

    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')
        email = request.data.get('email')
        first_name = request.data.get('first_name')
        last_name = request.data.get('last_name')

        location_lat = request.data.get('location_lat')
        location_lng = request.data.get('location_lng')

        serializer = BayiEkleSerializer(data=request.data)

        if not serializer.is_valid():
            response_data = {
                'detail': 'bayi eklenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

        # once default django useri olustur
        user = User()
        user.username = username
        user.password = hashers.make_password(password=password,
                                              salt=crypto.get_random_string(10),
                                              hasher=hashers.PBKDF2PasswordHasher())
        user.email = email
        user.first_name = first_name
        user.last_name = last_name
        user.date_joined = timezone.now()
        user.save()

        # olusturdugun useri bayi objesine bagla
        bayi = Bayi()
        bayi.user = user

        bayi.location_lat = location_lat
        bayi.location_lng = location_lng

        # bayi kaydet
        bayi.save()

        response_data = {
            'detail': 'bayi eklendi'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)


class BayiListele(APIView):

    def get(self, request):
        bayiler = Bayi.objects.order_by('-id')

        serializer = BayiSerializer(bayiler, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class BayiDetay(APIView):

    def get_object(self, pk):
        try:
            return Bayi.objects.get(pk=pk)
        except Bayi.DoesNotExist:
            raise Http404

    def get(self, request, pk):

        bayi = self.get_object(pk)
        serializer = BayiSerializer(bayi)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):

        bayi = self.get_object(pk)

        serializer = BayiSerializer(bayi, data=request.data)

        if serializer.is_valid():
            serializer.save()

            response_data = {
                'detail': 'bayi guncellendi'
            }
            return Response(data=response_data, status=status.HTTP_200_OK)

        else:
            response_data = {
                'detail': 'bayi guncellenemedi.',
                'errors': serializer.errors
            }
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):

        bayi = self.get_object(pk)

        bayi.delete()
        response_data = {
            'detail': 'bayi silindi.'
        }
        return Response(data=response_data, status=status.HTTP_200_OK)
