from rest_framework import serializers

from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User

from .models import *


class BayiEkleSerializer(serializers.Serializer):
    username = serializers.CharField(min_length=2,
                                     max_length=50,
                                     allow_blank=False,
                                     allow_null=False)

    password = serializers.CharField(min_length=3,
                                     max_length=50,
                                     allow_blank=False,
                                     allow_null=False)

    email = serializers.EmailField(allow_blank=False,
                                   allow_null=False)

    first_name = serializers.CharField()

    last_name = serializers.CharField()

    sehir = serializers.CharField()
    ilce = serializers.CharField()
    acik_adres = serializers.CharField()
    enlem = serializers.FloatField(allow_null=False)
    boylam = serializers.FloatField(allow_null=False)

    unvan = serializers.CharField()
    aciklama = serializers.CharField(max_length=512)

class BayiSerializer(serializers.ModelSerializer):
    enlem = serializers.ReadOnlyField()
    boylam = serializers.ReadOnlyField()
    class Meta:
        model = Bayi
        fields = '__all__'

class AdresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Adres
        fields = '__all__'

class UrunSerializer(serializers.ModelSerializer):
    class Meta:
        model = Urun
        fields = '__all__'

class MusteriSerializer(serializers.ModelSerializer):
    class Meta:
        model = Musteri
        fields = '__all__'

class StokSerializer(serializers.ModelSerializer):
    urun_adi = serializers.ReadOnlyField()

    class Meta:
        model = Stok
        fields = '__all__'

class SepetSerializerTek(serializers.ModelSerializer):
    alan_bayi_enlem = serializers.ReadOnlyField()
    alan_bayi_boylam = serializers.ReadOnlyField()
    teslimat_bayi_enlem = serializers.ReadOnlyField()
    teslimat_bayi_boylam = serializers.ReadOnlyField()
    teslimat_adresi_enlem = serializers.ReadOnlyField()
    teslimat_adresi_boylam = serializers.ReadOnlyField()

    class Meta:
        model = Sepet
        fields = '__all__'

class SepetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sepet
        fields = '__all__'

class SepetKalemiSerializer(serializers.ModelSerializer):
    class Meta:
        model = SepetKalemi
        fields = '__all__'