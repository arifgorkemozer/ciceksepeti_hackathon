from django.contrib.auth.models import User
from django.db import models

from enum import Enum

class DurumTyeps(Enum):
    H = 'Hazırlanıyor'
    K = 'Kargoda'
    T = 'Teslim edildi'

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)

class CinsiyetTypes(Enum):
    E = 'Erkek'
    K = 'Kadin'

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)

class Adres(models.Model):
    sehir = models.CharField(max_length=30, null=False, blank=False)
    ilce = models.CharField(max_length=30, null=False, blank=False)
    acik_adres = models.CharField(max_length=255, null=False, blank=False)
    enlem = models.FloatField('enlem', null=False, blank=False, default=0)
    boylam = models.FloatField('boylam', null=False, blank=False, default=0)

    def __str__(self):
        return '%s %s/%s' % (self.acik_adres, self.ilce, self.sehir)

class Bayi(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    unvan = models.CharField(max_length=256, null=False, blank=False)
    adres_id = models.ForeignKey(Adres, on_delete=models.PROTECT, null=True)
    aciklama = models.CharField(max_length=512, default='')
    bakiye = models.FloatField(null=False, blank=False, default=0)


    @property
    def enlem(self):
        return self.adres_id.enlem

    @property
    def boylam(self):
        return self.adres_id.boylam

    def __str__(self):
        return '%s - %s (%.02f TL)' % (self.unvan, self.user.username, self.bakiye)

class Urun(models.Model):
    urun_adi = models.CharField(max_length=128, blank=False, null=False)
    fiyat = models.FloatField(null=False, blank=False)
    aciklama = models.TextField(max_length=512, default='')
    resim_url = models.TextField(default='https://ae01.alicdn.com/kf/HTB1Luv1LXXXXXcJXXXXq6xXFXXXI/Glenna-Jean-Grey-Flower-Removable-Wall-Applique-Stickers-Decorative-Wall-Decals-56X56CM.jpg',
                                 blank=True, null=True)

    def __str__(self):
        return '%s - %s' % (self.urun_adi, self.aciklama[:20])

class Musteri(models.Model):
    adres_id = models.ForeignKey(Adres, on_delete=models.SET_NULL, null=True)

    isim = models.CharField(max_length=30, null=False, blank=False)
    musteri_kodu = models.CharField(max_length=30, null=False, blank=False, unique=True)
    yas = models.IntegerField(null=False, blank=False)
    cinsiyet = models.CharField(max_length=1, choices=CinsiyetTypes.choices(), default=CinsiyetTypes.E)

    def __str__(self):
        return '%s (%s) - %s' % (self.isim, self.musteri_kodu, self.adres_id.sehir)

class Stok(models.Model):
    bayi_id = models.ForeignKey(Bayi, on_delete=models.CASCADE, null=True)
    urun_id = models.ForeignKey(Urun, on_delete=models.CASCADE, null=True)

    miktar = models.IntegerField(null=False, blank=False)

    def __str__(self):
        return '%s (+%s)' % (self.urun_id, self.miktar)

    @property
    def urun_adi(self):
        return self.urun_id.urun_adi

class Sepet(models.Model):
    alan_bayi_id = models.ForeignKey(Bayi, related_name="alan_bayi_id", on_delete=models.SET_NULL, null=True)
    teslimat_bayi_id = models.ForeignKey(Bayi, related_name="teslimat_bayi_id", on_delete=models.SET_NULL, null=True)
    musteri_id = models.ForeignKey(Musteri, related_name="musteri_id", on_delete=models.SET_NULL, null=True)
    tekil_kod = models.CharField(max_length=16, null=False, blank=False)
    durum = models.CharField(max_length=1, choices=DurumTyeps.choices(), default=DurumTyeps.H)
    note = models.CharField(max_length=512, default='')
    siparis_tarihi = models.DateField(null=True, blank=True)
    teslimat_tarihi = models.DateField(null=True, blank=True)
    teslimat_adresi_id = models.ForeignKey(Adres, on_delete=models.SET_NULL, null=True)
    hedef_musteri_id = models.ForeignKey(Musteri, related_name="hedef_musteri_id", on_delete=models.SET_NULL, null=True)

    @property
    def alan_bayi_enlem(self):
        return self.alan_bayi_id.adres_id.enlem

    @property
    def alan_bayi_boylam(self):
        return self.alan_bayi_id.adres_id.boylam

    @property
    def teslimat_bayi_enlem(self):
        return self.teslimat_bayi_id.adres_id.enlem

    @property
    def teslimat_bayi_boylam(self):
        return self.teslimat_bayi_id.adres_id.boylam

    @property
    def teslimat_adresi_enlem(self):
        return self.teslimat_adresi_id.enlem

    @property
    def teslimat_adresi_boylam(self):
        return self.teslimat_adresi_id.boylam

class SepetKalemi(models.Model):
    sepet_id = models.ForeignKey(Sepet, on_delete=models.CASCADE, null=True)
    urun_id = models.ForeignKey(Urun, on_delete=models.CASCADE, null=True)

    miktar = models.IntegerField(null=False, blank=False)
    tutar = models.FloatField(null=False, blank=False)

    def __str__(self):
        return '%s %s-%s TL' % (self.urun_id, self.miktar, self.tutar)