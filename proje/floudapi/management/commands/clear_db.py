from django.contrib.auth.models import User

from django.core.management.base import BaseCommand
from django.contrib.auth import hashers
from django.utils import crypto
from django.utils import timezone
import re
import random

from floudapi.models import *


class Command(BaseCommand):
    help = 'Clear db'

    def handle(self, *args, **kwargs):
        Bayi.objects.all().delete()
        Adres.objects.all().delete()


