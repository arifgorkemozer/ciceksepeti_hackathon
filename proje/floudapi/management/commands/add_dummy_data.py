from django.contrib.auth.models import User

from django.core.management.base import BaseCommand
from django.contrib.auth import hashers
from django.utils import crypto
from django.utils import timezone
import re
import random
from .isimler import isimler
from floudapi.models import *
import string
from datetime import datetime
from datetime import timedelta
from django.db.models import Sum

import timeit
from django.db.transaction import commit

class Command(BaseCommand):
    help = 'Insert sample data'

    def create_bayi(self, username, password, email, fname, lname, unvan, adres, aciklama):
        u = User(username=username,
                 password=password,
                 email=email,
                 first_name=fname,
                 last_name=lname,
                 date_joined=timezone.now())

        u.password = hashers.make_password(password=u.password,
                              salt=crypto.get_random_string(10),
                              hasher=hashers.PBKDF2PasswordHasher())
        u.save()

        bayi = Bayi()
        bayi.user = u
        bayi.unvan = unvan
        bayi.adres_id = adres
        bayi.aciklama = aciklama
        bayi.save()
        return bayi

    def clear_all(self):
        Bayi.objects.all().delete()
        Adres.objects.all().delete()
        User.objects.filter(username__contains='Bayi_username_').delete()
        Stok.objects.all().delete()
        Urun.objects.all().delete()
        Musteri.objects.all().delete()
        Sepet.objects.all().delete()
        SepetKalemi.objects.all().delete()

    def handle(self, *args, **kwargs):
        start = timeit.default_timer()
        self.clear_all()
        print("silme bitti")

        bayi_sayisi = 30
        urun_cesidi = 10
        musteri_sayisi = 30
        sepet_sayisi = 5000
        adres_sayisi = bayi_sayisi + musteri_sayisi + 50

        sehirler = [("Ankara", 39.95, 32.82, 39.99, 32.74), ("İstanbul", 41.01, 28.91, 41.063, 28.81), ("İzmir", 38.37, 27.14, 38.40, 27.10)]
        ilceler = ["ilce 1", "ilce 2", "ilce 3"]

        adresler = []
        bayiler = []
        urunler = []
        musteriler = []
        sepetler = []
        sepetKalemleri = []
        stoklar = []

        for i in range(adres_sayisi):
            adres = Adres()
            if i == 0:
                sehir, x1, y1, x2, y2 = sehirler[0]
            else:
                sehir, x1, y1, x2, y2 = random.choice(sehirler)
            adres.sehir = sehir
            adres.ilce = random.choice(ilceler)
            adres.enlem = x2 + random.random() * (x1 - x2)
            adres.boylam = y2 + random.random() * (y1 - y2)
            adres.acik_adres = "%s - %s Sokak %s" % (sehir, adres.ilce, (i + 1))
            adresler.append(adres)
        Adres.objects.bulk_create(adresler)
        adresler = list(Adres.objects.all())

        print("Adresler eklendi. ", len(adresler))
        for i in range(bayi_sayisi):
            if i == 0:
                username = "testuser"
            else:
                username = "testuser%03d" % (i + 1)
            password = "123"
            email = username + "@ciceksepeti.com"
            fname = "fname %03d" % i
            lname = "lname %03d" % i
            unvan = "unvan %03d" % i
            adres = adresler.pop(0)
            aciklama = "dummy aciklama %03d" % (i + 1)
            bayi = self.create_bayi(username, password, email, fname, lname, unvan, adres, aciklama)
            bayi.bakiye = -1000 + random.randint(0, 2000)
            bayiler.append(bayi)

        print("Bayiler eklendi. ", len(bayiler))
        for i in range(urun_cesidi):
            urun = Urun()
            urun.urun_adi = "Urun adi %03d" % (i + 1)
            urun.aciklama = "Urun aciklama %03d" % (i + 1)
            urun.url = "https://cdn03.ciceksepeti.com/cicek/kc720123-1/M/fotograf-baskili-ahsap-donen-degirmen-cerceve-8-adet-fotografli-kc720123-1-1.jpg"
            urun.fiyat = random.randint(10, 100)
            urunler.append(urun)
        Urun.objects.bulk_create(urunler)
        urunler = list(Urun.objects.all())
        print("Urunler eklendi", len(urunler))

        for bayi in bayiler:
            for urun in urunler:
                miktar = random.randint(2, 20)
                stok = Stok()
                stok.bayi_id = bayi
                stok.urun_id = urun
                stok.miktar = miktar
                stoklar.append(stok)
        Stok.objects.bulk_create(stoklar)
        stoklar = list(Stok.objects.all())
        print("Stoklar eklendi", len(stoklar))

        for i in range(musteri_sayisi):
            musteri = Musteri()
            musteri.isim = random.choice(isimler)
            musteri.musteri_kodu = "musteri_%03d" % (i + 1)
            musteri.yas = random.randint(20, 60)
            musteri.cinsiyet = random.choice(["E", "K"])
            musteri.adres_id = adresler.pop(0)
            musteriler.append(musteri)
        Musteri.objects.bulk_create(musteriler)
        musteriler = list(Musteri.objects.all())
        print("Musteriler eklendi", len(musteriler))

        kalici_siparis_tarihi = datetime.now()
        kalici_teslimat_tarihi = datetime.now()
        for i in range(sepet_sayisi):
            sepet = Sepet()
            sepet.alan_bayi_id, sepet.teslimat_bayi_id = random.sample(bayiler, 2)

            musteri_id, hedef_musteri_id = random.sample(musteriler, 2)
            if random.random() > 0.5:
                sepet.musteri_id = musteri_id
            if random.random() > 0.5:
                sepet.hedef_musteri_id = hedef_musteri_id

            sepet.tekil_kod = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
            sepet.note = "Note Text %03d" % (i + 1)
            sepet.teslimat_adresi_id = random.choice(adresler)

            if i and random.random() < 0.1:
                siparis_tarihi = kalici_siparis_tarihi
                teslimat_tarihi = kalici_teslimat_tarihi
            else:
                siparis_tarihi = datetime.now() - timedelta(days=random.randint(1, 5 * 365))
                teslimat_tarihi = siparis_tarihi + timedelta(days=random.randint(0, 10))

                kalici_siparis_tarihi = siparis_tarihi
                kalici_teslimat_tarihi = teslimat_tarihi

            if teslimat_tarihi < datetime.now():
                durum = "T"
            else:
                durum = "H" if random.random() > 0.5 else "K"

            sepet.siparis_tarihi = siparis_tarihi
            sepet.teslimat_tarihi = teslimat_tarihi
            sepet.durum = durum
            sepetler.append(sepet)
        Sepet.objects.bulk_create(sepetler)
        sepetler = list(Sepet.objects.all())
        print("Sepetler eklendi ", len(sepetler))

        for sepet in sepetler:
            urun_sayisi = random.randint(1, 3)
            kalem_urunleri = random.sample(urunler, urun_sayisi)
            for urun in kalem_urunleri:
                kalem = SepetKalemi()
                kalem.urun_id = urun
                kalem.miktar = random.randint(1, 10)
                kalem.tutar = kalem.miktar * urun.fiyat
                kalem.sepet_id = sepet
                sepetKalemleri.append(kalem)
        SepetKalemi.objects.bulk_create(sepetKalemleri)
        sepetKalemleri = list(SepetKalemi.objects.all())

        print("Sepet Kalemleri eklendi. ", len(sepetKalemleri))

        end = timeit.default_timer()
        print("Elapsed time: ", end - start)

        for bayi in bayiler:
            alan_bayi_satislari = SepetKalemi.objects.filter(sepet_id__alan_bayi_id=bayi.id).aggregate(Sum('tutar'))["tutar__sum"]
            teslimat_satislari = SepetKalemi.objects.filter(sepet_id__teslimat_bayi_id=bayi.id).aggregate(Sum('tutar'))["tutar__sum"]

            cicek_sepeti_yuzdesi = 0.02
            borc = (alan_bayi_satislari / 2 - alan_bayi_satislari * cicek_sepeti_yuzdesi) / 2
            alacak = (teslimat_satislari / 2 - teslimat_satislari * cicek_sepeti_yuzdesi) / 2

            bayi.bakiye = alacak - borc
            bayi.save()
        print("bayi bakiyeleri hesaplandi")


