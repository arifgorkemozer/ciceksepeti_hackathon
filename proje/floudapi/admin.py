from django.contrib import admin
from .models import *

admin.site.register(Bayi)
admin.site.register(Adres)
admin.site.register(Stok)
admin.site.register(Urun)
admin.site.register(Musteri)
admin.site.register(Sepet)
admin.site.register(SepetKalemi)