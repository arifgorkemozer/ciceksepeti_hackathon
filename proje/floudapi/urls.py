from django.urls import path
from django.urls import include
from .views.bayi_views import *
from .views.adres_views import *
from .views.urun_view import *
from .views.musteri_views import *
from .views.stok_views import *
from .views.sepet_views import *
from .views.sepetkalemi_views import *

urlpatterns = [
    path('bayilogin/', BayiLogin.as_view()),
    path('bayiliste/', BayiListele.as_view()),
    path('bayiekle/', BayiEkle.as_view()),
    path('bayidetay/<int:pk>', BayiDetay.as_view()),
    path('uygunbayi/<int:pk>', UygunBayiBulma.as_view()),
    path('bayitavsiye/<int:pk>', BayiTavsiye.as_view()),

    path('adresliste/', AdresListele.as_view()),
    path('adresekle/', AdresEkle.as_view()),
    path('adresdetay/<int:pk>', AdresDetay.as_view()),

    path('urunliste/', UrunListele.as_view()),
    path('urunekle/', UrunEkle.as_view()),
    path('urundetay/<int:pk>', UrunDetay.as_view()),

    path('musteriliste/', MusteriListele.as_view()),
    path('musteriekle/', MusteriEkle.as_view()),
    path('musteridetay/<int:pk>', MusteriDetay.as_view()),

    path('stokliste/', StokListele.as_view()),
    path('stoklistebayi/<int:pk>', StokListeleBayi.as_view()),
    path('stokekle/', StokEkle.as_view()),
    path('stokdetay/<int:pk>', StokDetay.as_view()),

    path('sepetlistebayi/<int:pk>', SepetListeleBayi.as_view()),
    path('sepetliste/', SepetListele.as_view()),
    path('sepetekle/', SepetEkle.as_view()),
    path('sepetdetay/<int:pk>', SepetDetay.as_view()),

    path('sepetkalemiliste/', SepetKalemiListele.as_view()),
    path('sepetkalemilistesepet/<int:pk>', SepetKalemiListeleSepet.as_view()),


    path('sepetkalemiekle/', SepetKalemiEkle.as_view()),
    path('sepetkalemidetay/<int:pk>', SepetKalemiDetay.as_view())
]