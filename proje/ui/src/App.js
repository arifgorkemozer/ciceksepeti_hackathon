import React, {Component} from 'react';
import {withCookies} from 'react-cookie';

import LayoutAnon from './Layouts/LayoutAnon';
import LayoutBayi from './Layouts/LayoutBayi';

class App extends Component {

    state = {
        is_logged_in: false,
        bayi_id: "",
        bayi_username: "",
        bayi_unvan: "",
        bayi_aciklama: "",
        bayi_adres_id: "",
        bayi_bakiye: 0
    };

    updateStateForLogin = (loginResponse) => {
        // set cookie, update state
        const {cookies} = this.props;

        cookies.set("is_logged_in", loginResponse.is_logged_in, {path: '/'});
        cookies.set("bayi_id", loginResponse.bayi_id, {path: '/'});
        cookies.set("bayi_username", loginResponse.bayi_username, {path: '/'});
        cookies.set("bayi_unvan", loginResponse.bayi_unvan, {path: '/'});
        cookies.set("bayi_aciklama", loginResponse.bayi_aciklama, {path: '/'});
        cookies.set("bayi_adres_id", loginResponse.bayi_adres_id, {path: '/'});
        cookies.set("bayi_bakiye", loginResponse.bayi_bakiye, {path: '/'});

        this.setState({
            is_logged_in: loginResponse.is_logged_in,
            bayi_id: loginResponse.bayi_data,
            bayi_username: loginResponse.bayi_username,
            bayi_unvan: loginResponse.bayi_unvan,
            bayi_aciklama: loginResponse.bayi_aciklama,
            bayi_adres_id: loginResponse.bayi_adres_id,
            bayi_bakiye: loginResponse.bayi_bakiye,

        });
    };

    updateStateForLogout = () => {
        // delete cookie, clear state
        const {cookies} = this.props;

        cookies.remove("is_logged_in");
        cookies.remove("bayi_id");
        cookies.remove("bayi_username");
        cookies.remove("bayi_unvan");
        cookies.remove("bayi_aciklama");
        cookies.remove("bayi_adres_id");
        cookies.remove("bayi_bakiye");

        this.setState({
            is_logged_in: false,
            bayi_id: "",
            bayi_username: "",
            bayi_unvan: "",
            bayi_aciklama: "",
            bayi_adres_id: "",
            bayi_bakiye: 0,
        });
    };

    componentDidMount() {

        // read cookie, alter layout if logged in
        const {cookies} = this.props;
        const is_logged_in = cookies.get("is_logged_in") === "true";
        const bayi_id = cookies.get("bayi_id");
        const bayi_username = cookies.get("bayi_username");
        const bayi_unvan = cookies.get("bayi_unvan");
        const bayi_aciklama = cookies.get("bayi_aciklama");
        const bayi_adres_id = cookies.get("bayi_adres_id");
        const bayi_bakiye = cookies.get("bayi_bakiye");


        if (is_logged_in) {
            this.setState({
                is_logged_in: is_logged_in,
                bayi_id: bayi_id,
                bayi_username: bayi_username,
                bayi_unvan: bayi_unvan,
                bayi_aciklama: bayi_aciklama,
                bayi_adres_id: bayi_adres_id,
                bayi_bakiye: bayi_bakiye,
            });
        }
        else {
            this.setState({
                is_logged_in: false,
                bayi_id: "",
                bayi_username: "",
                bayi_unvan: "",
                bayi_aciklama: "",
                bayi_adres_id: "",
                bayi_bakiye: 0,
            });
        }
    }

    render() {

        return (
            <div className="App">

                {
                    this.state.is_logged_in ?
                        <LayoutBayi logoutStateUpdater={this.updateStateForLogout}/>
                        :
                        <LayoutAnon loginStateUpdater={this.updateStateForLogin}/>
                }

            </div>
        );
    }
}

export default withCookies(App);
