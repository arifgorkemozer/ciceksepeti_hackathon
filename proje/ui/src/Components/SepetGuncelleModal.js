import React, {Component} from 'react';
import axios from 'axios';
import {withCookies} from 'react-cookie';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Edit from '@material-ui/icons/Edit'
import Clear from '@material-ui/icons/Clear'


class SepetGuncelleModal extends Component {

    state = {
        modal_open: false,
        bayiler: [],
        adresler: [],
        musteriler: [],
        sepet_id: "",
        alan_bayi_id: "",
        teslimat_bayi_id: "",
        musteri_id: "",
        tekil_kod: "",
        durum: "",
        note: "",
        siparis_tarihi: "",
        teslimat_tarihi: "",
        teslimat_adresi_id: "",
        hedef_musteri_id: "",
    };

    openModal = () => {
        this.setState({
            modal_open: true
        });

        this.sendSepetGetRequest();
    };
    closeModal = () => {
        this.setState({
            modal_open: false,
            alan_bayi_id: "",
            teslimat_bayi_id: "",
            musteri_id: "",
            tekil_kod: "",
            durum: "",
            note: "",
            siparis_tarihi: "",
            teslimat_tarihi: "",
            teslimat_adresi_id: "",
            hedef_musteri_id: "",

        });

        this.props.parentUpdate();
    };

    onChangeAlanBayi = (event) => {

        this.setState({
            alan_bayi_id: event.target.value,
        });
    };

    onChangeTeslimatBayi = (event) => {

        this.setState({
            teslimat_bayi_id: event.target.value,
        });
    };

    onChangeMusteriId = (event) => {

        this.setState({
            musteri_id: event.target.value,
        });
    };

    onChangeTekilKod = (event) => {
        this.setState({
            tekil_kod: event.target.value
        })
    };

    onChangeDurum = (event) => {
        this.setState({
            durum: event.target.value
        })
    };

    onChangeNote = (event) => {
        this.setState({
            note: event.target.value
        })
    };

    onChangeHedefMusteriId = (event) => {

        this.setState({
            hedef_musteri_id: event.target.value,
        });
    };
    onChangeTeslimatAdresi = (event) => {

        this.setState({
            teslimat_adresi_id: event.target.value,
        });
    };

    onChangeSipTarih = (event) => {

        this.setState({
            siparis_tarihi: event.currentTarget.value,
        });
    };

    onChangeTeslimTarih = (event) => {
        this.setState({
            teslimat_tarihi: event.currentTarget.value,
        });
    };

    sendBayiListRequest = () => {
        axios.get('http://127.0.0.1:8000/bayiliste/')
            .then(response => {
                this.setState({
                    bayiler: response.data
                });
            });
    };

    sendAdresListRequest = () => {
        axios.get('http://127.0.0.1:8000/adresliste/')
            .then(response => {
                this.setState({
                    adresler: response.data
                });
            });
    };

    sendMusteriListRequest = () => {
        axios.get('http://127.0.0.1:8000/musteriliste/')
            .then(response => {
                this.setState({
                    musteriler: response.data
                });
            });
    };

    sendUpdateSepetRequest = () => {

        const {cookies} = this.props;
        const bayi_id = cookies.get("bayi_id");

        var requestData = {
            alan_bayi_id: bayi_id,
            teslimat_bayi_id: this.state.teslimat_bayi_id,
            musteri_id: this.state.musteri_id,
            tekil_kod: this.state.tekil_kod,
            durum: this.state.durum,
            note: this.state.note,

            teslimat_adresi_id: this.state.teslimat_adresi_id,
            hedef_musteri_id: this.state.hedef_musteri_id,
        };

        if (this.state.siparis_tarihi !== "") {
            requestData.siparis_tarihi = this.state.siparis_tarihi;
        }
        if (this.state.teslimat_tarihi !== "") {
            requestData.teslimat_tarihi = this.state.teslimat_tarihi;
        }

        var config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };

        axios.put('http://127.0.0.1:8000/sepetdetay/' + this.props.sepetId, requestData, config)
            .then(response => {
                this.closeModal();

            })
            .catch(error => {

                console.log(error.response);
            });

    };

    sendSepetGetRequest = () => {
        axios.get('http://127.0.0.1:8000/sepetdetay/' + this.props.sepetId)
            .then(response => {
                this.setState({
                    sepet_id: response.data.id,
                    alan_bayi_id: response.data.alan_bayi_id,
                    teslimat_bayi_id: response.data.teslimat_bayi_id,
                    musteri_id: response.data.musteri_id,
                    tekil_kod: response.data.tekil_kod,
                    durum: response.data.durum,
                    note: response.data.note,
                    siparis_tarihi: response.data.siparis_tarihi,
                    teslimat_tarihi: response.data.teslimat_tarihi,
                    teslimat_adresi_id: response.data.teslimat_adresi_id,
                    hedef_musteri_id: response.data.hedef_musteri_id,
                });
            });
    };

    componentDidMount() {
        this.sendBayiListRequest();
        this.sendAdresListRequest();
        this.sendMusteriListRequest();

    }

    render() {
        const {cookies} = this.props;
        const bayi_id = cookies.get("bayi_id");
        const bayi_username = cookies.get("bayi_username");
        const bayi_unvan = cookies.get("bayi_unvan");
        const bayi_aciklama = cookies.get("bayi_aciklama");
        const bayi_adres_id = cookies.get("bayi_adres_id");
        const bayi_bakiye = cookies.get("bayi_bakiye");
        return (
            <div>
                <Button color="primary" variant="contained" onClick={this.openModal}>
                    <Edit/>
                    SEPETİ GÜNCELLE
                </Button>

                <Dialog open={this.state.modal_open}
                        onClose={this.closeModal}
                        aria-labelledby="form-dialog-title"
                        maxWidth="sm" fullWidth={true}>

                    <DialogTitle id="form-dialog-title">
                        Sepet Güncelleme
                    </DialogTitle>

                    <DialogContent>

                        <div>
                            <div style={{display: 'block'}}>
                                <div>

                                    <FormControl style={{width: '250px'}}>
                                        <InputLabel htmlFor="teslimat_bayi_select">Teslimat Bayi</InputLabel>
                                        <Select value={this.state.teslimat_bayi_id}
                                                onChange={this.onChangeTeslimatBayi}
                                                input={<Input name="secili_teslimat_bayi" id="teslimat_bayi_input"/>}>
                                            {this.state.bayiler.map(
                                                bayi => {
                                                    return <MenuItem key={bayi.id} value={bayi.id}>
                                                        {bayi.id}
                                                    </MenuItem>
                                                }
                                            )}

                                        </Select>
                                    </FormControl>

                                </div>
                                <div>
                                    <FormControl style={{width: '200px'}}>
                                        <InputLabel htmlFor="musteri_id_select">Gönderen Müşteri</InputLabel>
                                        <Select value={this.state.musteri_id}
                                                onChange={this.onChangeMusteriId}
                                                input={<Input name="hedef_musteri_id" id="musteri_id_input"/>}>
                                            {this.state.musteriler.map(
                                                musteri => {
                                                    return <MenuItem key={musteri.id} value={musteri.id}>
                                                        {musteri.isim}
                                                    </MenuItem>
                                                }
                                            )}

                                        </Select>
                                    </FormControl>

                                    <FormControl style={{width: '200px'}}>
                                        <InputLabel htmlFor="hedef_musteri_id_select">Hedef Müşteri</InputLabel>
                                        <Select value={this.state.hedef_musteri_id}
                                                onChange={this.onChangeHedefMusteriId}
                                                input={<Input name="secili_hedef_musteri_id"
                                                              id="hedef_musteri_id_input"/>}>
                                            {this.state.musteriler.map(
                                                musteri => {
                                                    return <MenuItem key={musteri.id} value={musteri.id}>
                                                        {musteri.isim}
                                                    </MenuItem>
                                                }
                                            )}

                                        </Select>
                                    </FormControl>

                                    <TextField id="tekil_kod"
                                               label="Tekil Kod"
                                               onChange={this.onChangeTekilKod}
                                               value={this.state.tekil_kod}
                                               style={{width: '250px'}}
                                               margin="normal"/>

                                    <TextField id="note"
                                               label="Not"
                                               onChange={this.onChangeNote}
                                               value={this.state.note}
                                               style={{width: '500px'}}
                                               margin="normal"/>
                                </div>
                                <div>
                                    <FormControl style={{width: '200px'}}>
                                        <InputLabel htmlFor="durum_select">Durum</InputLabel>
                                        <Select value={this.state.durum}
                                                onChange={this.onChangeDurum}
                                                input={<Input name="secili_durum" id="durum_input"/>}>
                                            <MenuItem key="H" value="H">Hazırlanıyor</MenuItem>
                                            <MenuItem key="K" value="K">Kargoda</MenuItem>
                                            <MenuItem key="T" value="T">Teslim edildi</MenuItem>
                                        </Select>
                                    </FormControl>


                                    <FormControl style={{width: '200px'}}>
                                        <InputLabel htmlFor="teslimat_adres_id">Teslimat Adresi</InputLabel>
                                        <Select value={this.state.teslimat_adresi_id}
                                                onChange={this.onChangeTeslimatAdresi}
                                                input={<Input name="secili_teslimat_adres" id="teslimat_adres_input"/>}>
                                            {this.state.adresler.map(
                                                adres => {
                                                    return <MenuItem key={adres.id} value={adres.id}>
                                                        {adres.acik_adres}
                                                    </MenuItem>
                                                }
                                            )}

                                        </Select>
                                    </FormControl>
                                </div>
                                <div>
                                    <TextField id="siparis_tarih"
                                               label="Sipariş tarihi"
                                               onChange={this.onChangeSipTarih}
                                               value={this.state.siparis_tarihi}
                                               type="date"
                                               InputLabelProps={{
                                                   shrink: true,
                                               }}
                                               margin="normal"/>

                                    <TextField id="teslimat_tarih"
                                               label="Teslimat tarihi"
                                               onChange={this.onChangeTeslimTarih}
                                               value={this.state.teslimat_tarihi}
                                               type="date"
                                               InputLabelProps={{
                                                   shrink: true,
                                               }}
                                               margin="normal"/>
                                </div>


                            </div>
                        </div>

                    </DialogContent>

                    <DialogActions>
                        <div style={{marginBottom: '20px', marginRight: '20px'}}>
                            <Button variant="contained"
                                    onClick={this.closeModal}>
                                <Clear/>
                                İptal
                            </Button>
                            <Button variant="contained"
                                    color="primary"
                                    onClick={this.sendUpdateSepetRequest}>
                                <Edit/>
                                GÜNCELLE
                            </Button>
                        </div>
                    </DialogActions>

                </Dialog>
            </div>

        )

    }

}


export default withCookies(SepetGuncelleModal);