import React, {Component} from 'react';
import axios from 'axios';
import {withCookies} from 'react-cookie';


import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from "@material-ui/core/Button/Button";
import CardGiftcard from "@material-ui/icons/CardGiftcard";
import Add from "@material-ui/icons/Add";
import Clear from "@material-ui/icons/Clear";

class UrunEkleModal extends Component {

    state = {
        modal_open: false,
        urun_adi: "",
        fiyat: 0,
        aciklama: "",
        resim_url: "",

    };


    openModal = () => {
        this.setState({
            modal_open: true,
            urun_adi: "",
            fiyat: 0,
            aciklama: "",
            resim_url: "",
        });
    };
    closeModal = () => {
        this.setState({
            modal_open: false,
            urun_adi: "",
            fiyat: 0,
            aciklama: "",
            resim_url: "",
        });

        this.props.parentUpdate();
    };

    onChangeUrunAdi = (event) => {
        this.setState({
            urun_adi: event.currentTarget.value,
        });
    };

    onChangeFiyat = (event) => {
        this.setState({
            fiyat: event.currentTarget.value,
        });
    };

    onChangeAciklama = (event) => {
        this.setState({
            aciklama: event.currentTarget.value,
        });
    };

    onChangeResim = (event) => {
        this.setState({
            resim_url: event.currentTarget.value,
        });
    };


    sendAddNewUrunRequest = () => {


        var requestData = {
            'urun_adi': this.state.urun_adi,
            'fiyat': this.state.fiyat,
            'aciklama': this.state.aciklama,
            'resim_url': this.state.resim_url
        };

        var config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };

        axios.post('http://127.0.0.1:8000/urunekle/', requestData, config)
            .then(response => {
                this.closeModal();

            })
            .catch(error => {

                console.log(error.response);
            });
    };


    render() {

        return (

            <div>
                <Button color="secondary" variant="contained" onClick={this.openModal}>
                    <CardGiftcard/>
                    YENİ ÜRÜN TANIMLA
                </Button>

                <Dialog open={this.state.modal_open}
                        onClose={this.closeModal}
                        aria-labelledby="form-dialog-title"
                        maxWidth="sm" fullWidth={true}>

                    <DialogTitle id="form-dialog-title">
                        Ürün Tanımlama
                    </DialogTitle>

                    <DialogContent>

                        <div>
                            <div style={{display: 'block'}}>
                                <div>
                                    <TextField id="urun_adi"
                                               label="Ürün adı"
                                               onChange={this.onChangeUrunAdi}
                                               value={this.state.urun_adi}
                                               style={{width: '200px'}}
                                               margin="normal"/>

                                    <TextField id="fiyat"
                                               label="Fiyat"
                                               onChange={this.onChangeFiyat}
                                               value={this.state.fiyat}
                                               type="number"
                                               style={{width: '200px', paddingLeft: '10px'}}
                                               margin="normal"/>
                                </div>
                                <div>
                                    <TextField id="aciklama"
                                               label="Açıklama"
                                               onChange={this.onChangeAciklama}
                                               value={this.state.aciklama}
                                               style={{width: '100%'}}
                                               margin="normal"/>
                                </div>
                                <div>
                                    <TextField id="resim_url"
                                               label="Resim URL"
                                               onChange={this.onChangeResim}
                                               value={this.state.resim_url}
                                               type="resim_url"
                                               style={{width: '100%'}}
                                               margin="normal"/>
                                </div>

                            </div>
                        </div>

                    </DialogContent>

                    <DialogActions>
                        <div style={{marginBottom: '20px', marginRight: '20px'}}>
                            <Button variant="contained"
                                    onClick={this.closeModal}>
                                <Clear/>
                                İptal
                            </Button>
                            <Button variant="contained"
                                    color="primary"
                                    onClick={this.sendAddNewUrunRequest}>
                                <Add/>
                                Tanımla
                            </Button>
                        </div>
                    </DialogActions>

                </Dialog>
            </div>


        );
    }
}

export default withCookies(UrunEkleModal);
