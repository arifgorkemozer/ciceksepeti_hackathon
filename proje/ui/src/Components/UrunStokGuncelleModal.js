import React, {Component} from 'react';
import axios from 'axios';
import {withCookies} from 'react-cookie';


import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from "@material-ui/core/Button/Button";
import Edit from "@material-ui/icons/Edit";
import Clear from "@material-ui/icons/Clear";

class UrunStokGuncelleModal extends Component {

    state = {
        modal_open: false,
        stok_id: 0,
        miktar: 0

    };


    openModal = () => {
        this.setState({
            modal_open: true,
            stok_id: this.props.stokId,
            miktar: this.props.miktar,
        });
    };
    closeModal = () => {
        this.setState({
            modal_open: false,
            stok_id: 0,
            miktar: 0

            
        });

        this.props.parentUpdate();
    };


    onChangeMiktar = (event) => {
        this.setState({
            miktar: event.currentTarget.value,
        });
    };



    sendStokUpdateRequest = () => {


        var requestData = {
            'miktar': this.state.miktar
        };

        var config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };

        axios.put('http://127.0.0.1:8000/stokdetay/' + this.state.stok_id , requestData, config)
            .then(response => {
                this.closeModal();

            })
            .catch(error => {

                console.log(error.response);
            });
    };


    render() {

        return (

            <div>
                <Button color="default" variant="contained" style={{marginLeft: '20px', width: '100px'}} onClick={this.openModal}>
                    <Edit/> GÜNCELLE
                </Button>

                <Dialog open={this.state.modal_open}
                        onClose={this.closeModal}
                        aria-labelledby="form-dialog-title"
                        maxWidth="xs" fullWidth={true}>

                    <DialogTitle id="form-dialog-title">
                        Ürün Stok Miktar Güncelleme
                    </DialogTitle>

                    <DialogContent>

                        <div>
                            <div style={{display: 'flex'}}>
                                

                                <TextField id="miktar"
                                           label="miktar"
                                           onChange={this.onChangeMiktar}
                                           value={this.state.miktar}
                                           type="number"
                                             style={{width: '500px'}}
                                           margin="normal"/>

                            </div>
                        </div>

                    </DialogContent>

                    <DialogActions>
                        <div style={{marginBottom: '20px', marginRight: '20px'}}>
                            <Button variant="contained"
                                    onClick={this.closeModal}>
                                <Clear/>
                                İptal
                            </Button>
                            <Button variant="contained"
                                    color="primary"
                                    onClick={this.sendStokUpdateRequest}>
                                <Edit/>
                                Güncelle
                            </Button>
                        </div>
                    </DialogActions>

                </Dialog>
            </div>


        );
    }
}

export default withCookies(UrunStokGuncelleModal);
