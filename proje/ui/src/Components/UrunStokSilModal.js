import React, {Component} from 'react';
import axios from 'axios';
import {withCookies} from 'react-cookie';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from "@material-ui/core/Button/Button";
import DialogContentText from '@material-ui/core/DialogContentText';
import Delete from "@material-ui/icons/Delete";
import Clear from "@material-ui/icons/Clear";

class UrunStokSilModal extends Component {

    state = {
        modal_open: false,
        stok_id: 0

    };


    openModal = () => {
        this.setState({
            modal_open: true,
            stok_id: this.props.stokId,
        });
    };
    closeModal = () => {
        this.setState({
            modal_open: false,
            stok_id: 0,


        });

        this.props.parentUpdate();
    };


    sendStokDeleteRequest = () => {



        axios.delete('http://127.0.0.1:8000/stokdetay/' + this.state.stok_id )
            .then(response => {
                this.closeModal();

            })
            .catch(error => {

                console.log(error.response);
            });
    };


    render() {

        return (

            <div>
                <Button color="secondary" variant="contained" style={{marginLeft: '20px', width: '100px'}} onClick={this.openModal}>
                    <Delete/>
                    SİL
                </Button>

                <Dialog open={this.state.modal_open}
                        onClose={this.closeModal}
                        aria-labelledby="form-dialog-title"
                        maxWidth="sm" fullWidth={true}>

                    <DialogTitle id="form-dialog-title">
                        Ürünü Stoktan Kaldır
                    </DialogTitle>

                    <DialogContent>
                        <DialogContentText>
                            Ürünü stoktan kaldırmak istediğinize emin misiniz?
                        </DialogContentText>
                    </DialogContent>

                    <DialogActions>
                        <div style={{marginBottom: '20px', marginRight: '20px'}}>
                            <Button variant="contained"
                                    onClick={this.closeModal}>
                                <Clear/>
                                İptal
                            </Button>
                            <Button variant="contained"
                                    color="secondary"
                                    onClick={this.sendStokDeleteRequest}>
                                <Delete/>
                                Kaldır
                            </Button>
                        </div>
                    </DialogActions>

                </Dialog>
            </div>


        );
    }
}

export default withCookies(UrunStokSilModal);
