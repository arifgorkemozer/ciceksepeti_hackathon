import React, {Component} from 'react';
import {GoogleApiWrapper, Map, Marker} from 'google-maps-react';
import {withCookies} from 'react-cookie';
import redMarker from '../Images/location-red.png'
import purpleMarker from '../Images/location-purple.png'
import greenMarker from '../Images/location-green.png'


import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from "@material-ui/core/Button/Button";
import LocationOn from '@material-ui/icons/LocationOn';
import Tooltip from "@material-ui/core/Tooltip/Tooltip";

import KeyboardBackspace from '@material-ui/icons/KeyboardBackspace';
import axios from "axios";

class UygunBayiBulModal extends Component {

    state = {
        modal_open: false,
        // map_center: {'lat': 0, 'lng': 0},
        alan_bayi_location: {},
        teslimat_adresi_location: {},
        teslimat_bayi_location: {}
    };

    openModal = () => {
        console.log(this.props.sepetData);
        this.setState({
            modal_open: true,
            alan_bayi_location: {
                'lat': this.props.sepetData.alan_bayi_enlem,
                'lng': this.props.sepetData.alan_bayi_boylam,
            },
            teslimat_adresi_location: {
                'lat': this.props.sepetData.teslimat_adresi_enlem,
                'lng': this.props.sepetData.teslimat_adresi_boylam
            }
        });

        axios.get('http://127.0.0.1:8000/uygunbayi/' + this.props.sepetData.id)
            .then(response => {
                console.log(response);
                this.setState({
                    teslimat_bayi_location: {
                        'lat': response.data.enlem,
                        'lng': response.data.boylam,
                    }
                });

                // this.calculateMapCenter();
            });


    };
    closeModal = () => {
        this.setState({
            modal_open: false,
            // map_center: {'lat': 0, 'lng': 0},
            alan_bayi_location: {},
            teslimat_adresi_location: {},
            teslimat_bayi_location: {}
        });
    };

    // calculateMapCenter = () => {
    //     console.log("heyyaaaaa");
    //     if (this.state.teslimat_bayi_location.lat !== undefined) {
    //         console.log("heyy");
    //         var center_lat = (this.state.alan_bayi_location.lat + this.state.teslimat_bayi_location.lat + this.state.teslimat_adresi_location.lat) / 3;
    //         var center_lng = (this.state.alan_bayi_location.lng + this.state.teslimat_bayi_location.lng + this.state.teslimat_adresi_location.lng) / 3;
    //
    //         this.setState({
    //             map_center: {
    //                 'lat': center_lat,
    //                 'lng': center_lng
    //             }
    //         });
    //     }
    //     else {
    //         var center_lat = (this.state.alan_bayi_location.lat + this.state.teslimat_adresi_location.lat) / 2;
    //         var center_lng = (this.state.alan_bayi_location.lng + this.state.teslimat_adresi_location.lng) / 2;
    //
    //         this.setState({
    //             map_center: {
    //                 'lat': center_lat,
    //                 'lng': center_lng
    //             }
    //         });
    //     }
    // };

    render() {

        const paperStyle = {
            width: '100%',
            height: '80%'
        };

        return (

            <div>
                <Tooltip title="En yakın bayiyi bul" placement="top">
                    <Button variant="contained" color="default"
                            onClick={this.openModal}>
                        <LocationOn/>
                        BAYİ BUL
                    </Button>
                </Tooltip>


                <Dialog open={this.state.modal_open}
                        onClose={this.closeModal}
                        aria-labelledby="form-dialog-title"
                        PaperProps={{
                            style: paperStyle
                        }}>

                    <div className="paper-inner">
                        <DialogTitle id="form-dialog-title">
                            Locations:
                        </DialogTitle>

                        <div className="location-selector-container">
                            <Map className="location-selector"
                                 google={this.props.google}
                                 zoom={11}
                                 initialCenter={this.props.givenCenter}>

                                <Marker icon={redMarker} position={this.state.alan_bayi_location}/>

                                <Marker icon={greenMarker} position={this.state.teslimat_adresi_location}/>

                                {
                                    this.state.teslimat_bayi_location.lat !== undefined &&
                                    <Marker icon={purpleMarker} position={this.state.teslimat_bayi_location}/>
                                }
                            </Map>

                        </div>

                        <DialogActions>
                            <div>
                                <Button variant="contained"
                                        onClick={this.closeModal}>
                                    <KeyboardBackspace/>
                                    GERİ DÖN
                                </Button>
                            </div>
                        </DialogActions>
                    </div>

                </Dialog>
            </div>


        );
    }
}


export default withCookies(GoogleApiWrapper({
    apiKey: ''
})(UygunBayiBulModal));