import React, {Component} from 'react';
import axios from 'axios';
import {withCookies} from 'react-cookie';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from "@material-ui/core/Button/Button";
import DialogContentText from '@material-ui/core/DialogContentText';

import Delete from '@material-ui/icons/Delete';
import Clear from '@material-ui/icons/Clear';

class SepetKalemiSilModal extends Component {

    state = {
        modal_open: false,

    };


    openModal = () => {
        this.setState({
            modal_open: true,

        });
    };
    closeModal = () => {
        this.setState({
            modal_open: false,


        });

        this.props.parentUpdate();
    };


    sendSepetKalemiSilRequest = () => {



        axios.delete('http://127.0.0.1:8000/sepetkalemidetay/' + this.props.sepetKalemiId )
            .then(response => {
                this.closeModal();

            })
            .catch(error => {

                console.log(error.response);
            });
    };


    render() {

        return (

            <div>
                <Button color="secondary" variant="contained" onClick={this.openModal}>
                    <Delete/>
                    KALDIR
                </Button>

                <Dialog open={this.state.modal_open}
                        onClose={this.closeModal}
                        aria-labelledby="form-dialog-title"
                        maxWidth="sm" fullWidth={true}>

                    <DialogTitle id="form-dialog-title">
                        Sepetten Ürün Kaldır
                    </DialogTitle>

                    <DialogContent>
                        <DialogContentText>
                            Bu ürünü sepetten kaldırmak istediğinize emin misiniz?
                        </DialogContentText>
                    </DialogContent>

                    <DialogActions>
                        <div style={{marginBottom: '20px', marginRight: '20px'}}>
                            <Button variant="contained"
                                    onClick={this.closeModal}>
                                <Clear/>
                                İptal
                            </Button>
                            <Button variant="contained"
                                    color="secondary"
                                    onClick={this.sendSepetKalemiSilRequest}>
                                <Delete/>
                                Kaldır
                            </Button>
                        </div>
                    </DialogActions>

                </Dialog>
            </div>


        );
    }
}

export default withCookies(SepetKalemiSilModal);
