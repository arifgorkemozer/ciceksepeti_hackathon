import React, {Component} from 'react';
import axios from 'axios';
import {withCookies} from 'react-cookie';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import SepetKalemiSilModal from "./SepetKalemiSilModal";
import SepetKalemiGuncelleModal from "./SepetKalemiGuncelleModal";
import SepetGuncelleModal from "./SepetGuncelleModal";
import Build from "@material-ui/icons/Build";
import KeyboardBackspace from '@material-ui/icons/KeyboardBackspace'
import SepetKalemiEkleModal from "./SepetKalemiEkleModal";
import UygunBayiBulModal from "./UygunBayiBulModal";


class SepetYonetModal extends Component {

    state = {
        modal_open: false,
        sepet_data: {},
        sepet_kalemleri: [],
        sepet_teslimat_adresi: ""
    };

    openModal = () => {
        this.setState({
            modal_open: true,

        });

        this.sendSepetGetRequest();
        this.sendSepetKalemleriListRequest();

    };


    closeModal = () => {
        this.setState({
            modal_open: false,

        });

    };


    sendSepetGetRequest = () => {
        axios.get('http://127.0.0.1:8000/sepetdetay/' + this.props.sepetId)
            .then(response => {
                this.setState({
                    sepet_data: response.data
                });
            });
    };

    sendSepetKalemleriListRequest = () => {
        axios.get('http://127.0.0.1:8000/sepetkalemilistesepet/' + this.props.sepetId)
            .then(response => {
                this.setState({
                    sepet_kalemleri: response.data
                });
            });
    };


    componentDidMount() {

    }

    calculateToplam = (sepet_kalemleri) => {

        var toplam = 0;

        for (var i = 0; i < sepet_kalemleri.length; i++) {
            toplam += sepet_kalemleri[i].tutar;
        }

        return toplam;
    };

    render() {
        const {cookies} = this.props;
        const bayi_id = cookies.get("bayi_id");
        const bayi_username = cookies.get("bayi_username");
        const bayi_unvan = cookies.get("bayi_unvan");
        const bayi_aciklama = cookies.get("bayi_aciklama");
        const bayi_adres_id = cookies.get("bayi_adres_id");
        const bayi_bakiye = cookies.get("bayi_bakiye");
        return (
            <div>
                <Button color="primary" variant="contained" style={{marginLeft: '20px', width: '100px'}}
                        onClick={this.openModal}>
                    <Build/>
                    YÖNET
                </Button>

                <Dialog open={this.state.modal_open}
                        onClose={this.closeModal}
                        aria-labelledby="form-dialog-title"
                        maxWidth="sm" fullWidth={true}>

                    <DialogTitle id="form-dialog-title">
                        Sepet Görünümü
                    </DialogTitle>

                    <DialogContent>
                        <div>
                            <div style={{display: 'block'}}>

                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <Typography variant="subtitle2">
                                                Teslimat Bayi No:
                                            </Typography>
                                        </td>
                                        <td>
                                            <Typography variant="body1">
                                                {this.state.sepet_data.teslimat_bayi_id}
                                            </Typography>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Typography variant="subtitle2">
                                                Müşteri No:
                                            </Typography>
                                        </td>
                                        <td>
                                            <Typography variant="body1">
                                                {this.state.sepet_data.musteri_id}
                                            </Typography>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Typography variant="subtitle2">
                                                Hedef Müşteri No:
                                            </Typography>
                                        </td>
                                        <td>
                                            <Typography variant="body1">
                                                {this.state.sepet_data.hedef_musteri_id}
                                            </Typography>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Typography variant="subtitle2">
                                                Durum:
                                            </Typography>
                                        </td>
                                        <td>
                                            <Typography variant="body1">
                                                {this.state.sepet_data.durum}
                                            </Typography>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Typography variant="subtitle2">
                                                Note:
                                            </Typography>
                                        </td>
                                        <td>
                                            <Typography variant="body1">
                                                {this.state.sepet_data.note}
                                            </Typography>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Typography variant="subtitle2">
                                                Sipariş Tarihi:
                                            </Typography>
                                        </td>
                                        <td>
                                            <Typography variant="body1">
                                                {this.state.sepet_data.siparis_tarihi}
                                            </Typography>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Typography variant="subtitle2">
                                                Teslimat Tarihi:
                                            </Typography>
                                        </td>
                                        <td>
                                            <Typography variant="body1">
                                                {this.state.sepet_data.teslimat_tarihi}
                                            </Typography>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Typography variant="subtitle2">
                                                Teslimat Adresi:
                                            </Typography>
                                        </td>
                                        <td>
                                            <Typography variant="body1">
                                                {this.state.sepet_data.teslimat_adresi_id}
                                            </Typography>

                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                {this.state.sepet_kalemleri.length === 0 ?
                                    <Typography variant="subtitle1">
                                        *** Sepette henüz ürün yok.
                                    </Typography>
                                    :
                                    <div>
                                        <table>

                                            <thead>
                                            <tr>
                                                <th>
                                                    <Typography variant="subtitle2">
                                                        Ürün Adı
                                                    </Typography>
                                                </th>
                                                <th>
                                                    <Typography variant="subtitle2">
                                                        Ürün Fiyatı
                                                    </Typography>
                                                </th>
                                                <th>
                                                    <Typography variant="subtitle2">
                                                        Ürün Miktarı
                                                    </Typography>
                                                </th>
                                                <th>
                                                    <Typography variant="subtitle2">
                                                        Tutar
                                                    </Typography>
                                                </th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            {this.state.sepet_kalemleri.map(
                                                sepet_kalemi => {
                                                    return <tr key={sepet_kalemi.id}>
                                                        <td style={{
                                                            paddingLeft: '10px',
                                                            paddingRight: '10px'
                                                        }}>{sepet_kalemi.urun_adi}</td>
                                                        <td style={{
                                                            paddingLeft: '10px',
                                                            paddingRight: '10px'
                                                        }}>{sepet_kalemi.urun_fiyat}</td>
                                                        <td style={{
                                                            paddingLeft: '10px',
                                                            paddingRight: '10px'
                                                        }}>{sepet_kalemi.miktar}</td>
                                                        <td style={{
                                                            paddingLeft: '10px',
                                                            paddingRight: '10px'
                                                        }}>{sepet_kalemi.tutar}</td>

                                                        <td><SepetKalemiGuncelleModal sepetKalemiId={sepet_kalemi.id}
                                                                                      fiyat={sepet_kalemi.urun_fiyat}
                                                                                      miktar={sepet_kalemi.miktar}
                                                                                      parentUpdate={this.sendSepetKalemleriListRequest}/>
                                                        </td>
                                                        <td><SepetKalemiSilModal sepetKalemiId={sepet_kalemi.id}
                                                                                 parentUpdate={this.sendSepetKalemleriListRequest}/>
                                                        </td>

                                                    </tr>
                                                }
                                            )}
                                            </tbody>

                                        </table>
                                        <Typography variant="subtitle1" style={{marginLeft: '300px'}}>
                                            Toplam: {this.calculateToplam(this.state.sepet_kalemleri)}
                                        </Typography>
                                    </div>
                                }

                                <SepetKalemiEkleModal sepetId={this.props.sepetId}
                                                      parentUpdate={() => {
                                                          this.sendSepetGetRequest();
                                                          this.sendSepetKalemleriListRequest();
                                                          this.props.parentUpdate();
                                                      }}/>

                                <UygunBayiBulModal sepetData={this.state.sepet_data}
                                                   givenCenter={
                                                       {
                                                           'lat': this.state.sepet_data.teslimat_adresi_enlem,
                                                           'lng': this.state.sepet_data.teslimat_adresi_boylam,
                                                       }

                                                   }

                                />

                            </div>
                        </div>

                    </DialogContent>

                    <DialogActions>
                        <div style={{marginBottom: '20px', marginRight: '20px'}}>
                            <Button variant="contained"
                                    onClick={this.closeModal}>
                                <KeyboardBackspace/>
                                GERİ DÖN
                            </Button>
                            <SepetGuncelleModal sepetId={this.props.sepetId}
                                                parentUpdate={() => {
                                                    this.sendSepetGetRequest();
                                                    this.sendSepetKalemleriListRequest();
                                                    this.props.parentUpdate();
                                                }}/>
                        </div>
                    </DialogActions>

                </Dialog>
            </div>

        )

    }

}


export default withCookies(SepetYonetModal);