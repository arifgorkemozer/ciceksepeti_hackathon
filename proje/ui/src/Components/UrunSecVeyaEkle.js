import React, {Component} from 'react';
import axios from 'axios';
import {withCookies} from 'react-cookie';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from "@material-ui/core/Grid/Grid";

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import UrunEkleModal from "./UrunEkleModal";
import GetApp from "@material-ui/icons/GetApp";

class UrunSecVeyaEkle extends Component {

    state = {
        urunler: [],

        secili_urun: "",
        miktar: 0
    };

    onChangeUrun = (event) => {

        this.setState({
            secili_urun: event.target.value,
        });
    };

    onChangeMiktar = (event) => {
        this.setState({
            miktar: event.target.value
        })
    };

    sendUrunListRequest = () => {
        axios.get('http://127.0.0.1:8000/urunliste/')
            .then(response => {
                this.setState({
                    urunler: response.data
                });
            });
    };


    sendAddToStock = () => {
        const {cookies} = this.props;
        const bayi_id = cookies.get("bayi_id");

        var requestData = {
            'bayi_id': bayi_id,
            'urun_id': this.state.secili_urun,
            'miktar': this.state.miktar
        };

        var config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };

        axios.post('http://127.0.0.1:8000/stokekle/', requestData, config)
            .then(response => {
                this.setState({
                    secili_urun: "",
                    miktar: 0
                });

                this.props.parentUpdate();

            })
            .catch(error => {

                console.log(error.response);
            });
    };

    componentDidMount() {
        this.sendUrunListRequest();
    }

    render() {
        const {cookies} = this.props;
        const bayi_id = cookies.get("bayi_id");
        const bayi_username = cookies.get("bayi_username");
        const bayi_unvan = cookies.get("bayi_unvan");
        const bayi_aciklama = cookies.get("bayi_aciklama");
        const bayi_adres_id = cookies.get("bayi_adres_id");
        const bayi_bakiye = cookies.get("bayi_bakiye");
        return (
            <div>
                <Grid item xs={8}>

                    <FormControl style={{width: '250px', marginLeft: '20px'}}>
                        <InputLabel htmlFor="urun_select">Ürün</InputLabel>
                        <Select value={this.state.secili_urun}
                                onChange={this.onChangeUrun}
                                input={<Input name="secili_urun" id="urun_select_input"/>}>
                            {this.state.urunler.map(
                                urun => {
                                    return <MenuItem key={urun.id} value={urun.id}>
                                        {urun.urun_adi} ({urun.fiyat} TL)
                                    </MenuItem>
                                }
                            )}

                        </Select>
                    </FormControl>

                    <TextField
                        variant="outlined"
                        id="miktar"
                        label="Miktar"
                        value={this.state.miktar}
                        onChange={this.onChangeMiktar}
                        margin="normal"
                        style={{marginLeft: '80px'}}/>


                    <Button variant="contained"
                            color="primary"
                            onClick={() => this.sendAddToStock()}
                            style={{marginLeft: '80px'}}>
                        <GetApp/>
                        STOKLARA EKLE
                    </Button>

                </Grid>
                <Grid item xs={4}>
                    <UrunEkleModal parentUpdate={this.sendUrunListRequest}/>
                </Grid>


            </div>
        )

    }

}


export default withCookies(UrunSecVeyaEkle);