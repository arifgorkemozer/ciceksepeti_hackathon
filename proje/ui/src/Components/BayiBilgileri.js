import React, {Component} from 'react';
import axios from 'axios';
import {withCookies} from 'react-cookie';
import Typography from '@material-ui/core/Typography';
import bayi_icon from "../Images/bayi-icon.png";

class BayiBilgileri extends Component {

    state = {
        bayi_adresi: ""
    };

    sendFullAddressRequest = () => {
        const {cookies} = this.props;
        const bayi_adres_id = cookies.get("bayi_adres_id");

        axios.get('http://127.0.0.1:8000/adresdetay/' + bayi_adres_id)
            .then(response => {
                this.setState({
                    bayi_adresi: response.data.acik_adres + ", " + response.data.ilce + "/" + response.data.sehir
                })
            })
            .catch(error => {

                console.log(error.response);
            });
    };

    componentDidMount() {
        this.sendFullAddressRequest();
    }

    render() {
        const {cookies} = this.props;
        const bayi_id = cookies.get("bayi_id");
        const bayi_username = cookies.get("bayi_username");
        const bayi_unvan = cookies.get("bayi_unvan");
        const bayi_aciklama = cookies.get("bayi_aciklama");
        const bayi_adres_id = cookies.get("bayi_adres_id");
        const bayi_bakiye = cookies.get("bayi_bakiye");
        return (
            <div>
                <div>
                    <img src={bayi_icon} style={{width: '200px'}}
                         alt="bayi icon"
                    />
                </div>
                <Typography variant="h6">
                    Bayi Detayları
                </Typography>

                <table>
                    <tbody>
                        <tr>
                            <td>
                                <Typography variant="body2">
                                    Bayi Kayıt No:
                                </Typography>
                            </td>
                            <td>
                                <Typography variant="body1" style={{paddingLeft: '100px'}}>
                                    {bayi_id}
                                </Typography>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Typography variant="body2">
                                    Bayi Kullanıcı Adı:
                                </Typography>
                            </td>
                            <td>
                                <Typography variant="body1" style={{paddingLeft: '100px'}}>
                                    {bayi_username}
                                </Typography>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Typography variant="body2">
                                    Bayi Unvan:
                                </Typography>
                            </td>
                            <td>
                                <Typography variant="body1" style={{paddingLeft: '100px'}}>
                                    {bayi_unvan}
                                </Typography>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Typography variant="body2">
                                    Bayi Açıklaması:
                                </Typography>
                            </td>
                            <td>
                                <Typography variant="body1" style={{paddingLeft: '100px'}}>
                                    {bayi_aciklama}
                                </Typography>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Typography variant="body2">
                                    Bayi Kullanıcı Adı:
                                </Typography>
                            </td>
                            <td>
                                <Typography variant="body1" style={{paddingLeft: '100px'}}>
                                    {bayi_username}
                                </Typography>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Typography variant="body2">
                                    Bayi Adresi:
                                </Typography>
                            </td>
                            <td>
                                <Typography variant="body1" style={{paddingLeft: '100px'}}>
                                    {this.state.bayi_adresi}
                                </Typography>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Typography variant="body2">
                                    Bakiye (TL):
                                </Typography>
                            </td>
                            <td>
                                <Typography variant="body1" style={{paddingLeft: '100px'}}>
                                    {bayi_bakiye}
                                </Typography>
                            </td>
                        </tr>
                    </tbody>
                </table>


            </div>
        )

    }

}


export default withCookies(BayiBilgileri);