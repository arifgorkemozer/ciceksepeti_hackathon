import React, {Component} from 'react';
import axios from 'axios';
import {withCookies} from 'react-cookie';


import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from "@material-ui/core/Button/Button";
import Add from '@material-ui/icons/Add'
import Clear from '@material-ui/icons/Clear'
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import Input from "@material-ui/core/Input/Input";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import FormControl from "@material-ui/core/FormControl/FormControl";

class SepetKalemiEkleModal extends Component {

    state = {
        modal_open: false,
        urunler: [],
        urun_id: "",
        tutar: 0,
        miktar: 0

    };


    openModal = () => {
        this.setState({
            modal_open: true,
            urun_id: "",
            tutar: 0,
            miktar: 0
        });

        this.sendUrunListRequest();
    };
    closeModal = () => {
        this.setState({
            modal_open: false,
            urunler: [],
            urun_id: "",
            tutar: 0,
            miktar: 0


        });

        this.props.parentUpdate();
    };


    onChangeUrun = (event) => {

        this.setState({
            urun_id: event.target.value,
        });
    };

    onChangeMiktar = (event) => {
        this.setState({
            miktar: event.currentTarget.value,
        });
    };
    sendUrunListRequest = () => {
        axios.get('http://127.0.0.1:8000/urunliste/')
            .then(response => {
                this.setState({
                    urunler: response.data
                });
            });
    };

    sendSepetKalemiAddRequest = () => {


        var requestData = {
            'sepet_id': this.props.sepetId,
            'urun_id': this.state.urun_id,
            'miktar': parseInt(this.state.miktar),
            'tutar': 0
        };
        console.log(requestData);
        var config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };

        axios.post('http://127.0.0.1:8000/sepetkalemiekle/', requestData, config)
            .then(response => {
                this.closeModal();

            })
            .catch(error => {

                console.log(error.response);
            });
    };


    render() {

        return (

            <div>
                <Button color="primary" variant="contained" onClick={this.openModal}>
                    <Add/>
                    EKLE
                </Button>

                <Dialog open={this.state.modal_open}
                        onClose={this.closeModal}
                        aria-labelledby="form-dialog-title"
                        maxWidth="xs" fullWidth={true}>

                    <DialogTitle id="form-dialog-title">
                        Sepet Kalemi Ekleme
                    </DialogTitle>

                    <DialogContent>

                        <div>
                            <div style={{display: 'block'}}>

                                <div>
                                    <FormControl style={{width: '250px', marginLeft: '20px'}}>
                                        <InputLabel htmlFor="urun_select">Ürün</InputLabel>
                                        <Select value={this.state.urun_id}
                                                onChange={this.onChangeUrun}
                                                input={<Input name="secili_urun" id="urun_select_input"/>}>
                                            {this.state.urunler.map(
                                                urun => {
                                                    return <MenuItem key={urun.id} value={urun.id}>
                                                        {urun.urun_adi} ({urun.fiyat} TL)
                                                    </MenuItem>
                                                }
                                            )}

                                        </Select>
                                    </FormControl>
                                </div>
                                <div>
                                    <TextField id="miktar"
                                               label="Miktar"
                                               onChange={this.onChangeMiktar}
                                               value={this.state.miktar}
                                               type="number"
                                               style={{width: '250px', marginLeft: '20px'}}
                                               margin="normal"/>
                                </div>
                            </div>
                        </div>

                    </DialogContent>

                    <DialogActions>
                        <div style={{marginBottom: '20px', marginRight: '20px'}}>
                            <Button variant="contained"
                                    onClick={this.closeModal}>
                                <Clear/>
                                İptal
                            </Button>
                            <Button variant="contained"
                                    color="primary"
                                    onClick={this.sendSepetKalemiAddRequest}>
                                <Add/>
                                Ekle
                            </Button>
                        </div>
                    </DialogActions>

                </Dialog>
            </div>


        );
    }
}

export default withCookies(SepetKalemiEkleModal);
