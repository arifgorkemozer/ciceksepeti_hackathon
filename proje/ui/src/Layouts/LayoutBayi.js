import React, {Component} from 'react';
import axios from 'axios';
import {withCookies} from 'react-cookie';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid/Grid";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Tabs from "@material-ui/core/Tabs/Tabs";
import Tab from "@material-ui/core/Tab/Tab";

import './Layout.css'
import UrunSecVeyaEkle from "../Components/UrunSecVeyaEkle";
import UrunStokGuncelleModal from "../Components/UrunStokGuncelleModal";
import UrunStokSilModal from "../Components/UrunStokSilModal";
import BayiBilgileri from "../Components/BayiBilgileri";
import SepetEkleModal from "../Components/SepetEkleModal";
import SepetGoruntuleModal from "../Components/SepetGoruntuleModal";
import SepetYonetModal from "../Components/SepetYonetModal";
import SepetSilModal from "../Components/SepetSilModal";
import KeyboardBackspace from "@material-ui/icons/KeyboardBackspace";
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import orange from '@material-ui/core/colors/orange';

class LayoutBayi extends Component {

    state = {
        selected_tab: 0,
        stoklar: [],
        tavsiye_stoklar: [],
        sepetler: []
    };

    onTabChanged = (event, value) => {

        // profil ve siparis al
        if (value === 0) {
            this.sendSepetListRequest();
        }

        // stoklari al
        else if (value === 1) {

            this.sendStokListRequest();
            this.sendStokTavsiyeRequest();
        }
        // other
        else {

        }

        this.setState({
            selected_tab: value
        });
    };

    sendStokListRequest = () => {

        const {cookies} = this.props;
        const bayi_id = cookies.get("bayi_id");

        axios.get('http://127.0.0.1:8000/stoklistebayi/' + bayi_id)
            .then(response => {
                this.setState({
                    stoklar: response.data
                });
            });
    };

    sendStokTavsiyeRequest = () => {
        const {cookies} = this.props;
        const bayi_id = cookies.get("bayi_id");

        axios.get('http://127.0.0.1:8000/bayitavsiye/' + bayi_id)
            .then(response => {
                this.setState({
                    tavsiye_stoklar: response.data
                });


            });
    };

    sendSepetListRequest = () => {

        const {cookies} = this.props;
        const bayi_id = cookies.get("bayi_id");

        axios.get('http://127.0.0.1:8000/sepetlistebayi/' + bayi_id)
            .then(response => {
                this.setState({
                    sepetler: response.data
                });
            });
    };

    getOnerilenStok = (urun_id) => {
        for (var i = 0; i < this.state.tavsiye_stoklar.length; i++) {
            if (urun_id == this.state.tavsiye_stoklar[i].urun_id) {
                return this.state.tavsiye_stoklar[i].result;
            }
        }
    };

    componentDidMount() {
        this.sendSepetListRequest();
    }

    render() {
        const theme = createMuiTheme({
            palette: {
                primary: {main: green[500]},
                secondary: {main: orange[500]},
            },
            typography: {useNextVariants: true},
        });
        const {cookies} = this.props;
        const bayi_id = cookies.get("bayi_id");
        const bayi_username = cookies.get("bayi_username");
        const bayi_unvan = cookies.get("bayi_unvan");
        const bayi_aciklama = cookies.get("bayi_aciklama");
        const bayi_adres_id = cookies.get("bayi_adres_id");
        const bayi_bakiye = cookies.get("bayi_bakiye");
        return <div>
            <Grid className="layout-fullpage-container" container spacing={24}>

                <Grid item xs={12}>
                    <div style={{paddingTop: '20px', paddingBottom: '20px'}}>
                        <Button variant="contained"
                                color="primary"
                                onClick={() => this.props.logoutStateUpdater()}>
                            <KeyboardBackspace/>
                            Çıkış
                        </Button>
                    </div>
                    <div>
                        <AppBar position="static">
                            <Tabs value={this.state.selected_tab}
                                  onChange={this.onTabChanged}
                                  centered>
                                <Tab label="GENEL BAKIŞ"/>
                                <Tab label="STOK YÖNETİMİ"/>
                                <Tab label="DİĞER"/>
                            </Tabs>
                        </AppBar>
                    </div>
                </Grid>

                {/* ANA SAYFA */}
                {this.state.selected_tab === 0 &&
                <Grid item xs={4}>
                    <BayiBilgileri/>
                </Grid>
                }
                {/* ANA SAYFA */}
                {this.state.selected_tab === 0 &&
                <Grid item xs={8}>
                    <div style={{paddingBottom: '50px'}}>
                        <SepetEkleModal parentUpdate={this.sendSepetListRequest}/>
                    </div>
                    <Typography variant="h6">
                        Sepetlerim
                    </Typography>
                    {this.state.sepetler.length === 0 ?
                        <Typography variant="subtitle1">
                            Henüz sipariş almadınız. Sipariş aldıkça sepetleriniz görüntülenecektir.
                        </Typography>
                        :
                        <table>

                            <tbody>
                            <tr>
                                <th>
                                    <Typography variant="subheading">
                                        Alan Bayi No.
                                    </Typography>
                                </th>
                                <th>
                                    <Typography variant="subheading">
                                        Teslimat Bayi No.
                                    </Typography>
                                </th>
                                <th>
                                    <Typography variant="subheading">
                                        Müşteri No.
                                    </Typography>
                                </th>
                                <th>
                                    <Typography variant="subheading">
                                        Durum
                                    </Typography>
                                </th>
                                <th>
                                    <Typography variant="subheading">
                                        Sipariş Tarihi
                                    </Typography>
                                </th>
                            </tr>
                            {this.state.sepetler.map(
                                sepet => {
                                    return <tr key={sepet.id}>
                                        <td style={{
                                            paddingLeft: '10px',
                                            paddingRight: '10px'
                                        }}>{sepet.alan_bayi_id}</td>
                                        <td style={{
                                            paddingLeft: '10px',
                                            paddingRight: '10px'
                                        }}>{sepet.teslimat_bayi_id}</td>
                                        <td style={{
                                            paddingLeft: '10px',
                                            paddingRight: '10px'
                                        }}>{sepet.musteri_id ? sepet.musteri_id : "Yok"}</td>
                                        <td style={{paddingLeft: '10px', paddingRight: '10px'}}>{sepet.durum}</td>
                                        <td style={{
                                            paddingLeft: '10px',
                                            paddingRight: '10px'
                                        }}>{sepet.siparis_tarihi}</td>
                                        <td>
                                            {sepet.alan_bayi_id == bayi_id ?
                                                <div style={{display: "flex"}}>
                                                    <SepetYonetModal sepetId={sepet.id}
                                                                     parentUpdate={this.sendSepetListRequest}/>
                                                    <SepetSilModal sepetId={sepet.id}
                                                                   parentUpdate={this.sendSepetListRequest}/>
                                                </div>
                                                :
                                                <SepetGoruntuleModal sepetId={sepet.id}/>
                                            }


                                        </td>
                                    </tr>
                                }
                            )}
                            </tbody>

                        </table>
                    }
                </Grid>
                }

                {/*STOK YÖNETİMİ*/}
                {this.state.selected_tab === 1 &&
                <Grid item xs={12}>
                    <UrunSecVeyaEkle parentUpdate={() => {
                        this.sendStokTavsiyeRequest();
                        this.sendStokListRequest();
                    }}/>
                </Grid>
                }
                {/*STOK YÖNETİMİ*/}
                {this.state.selected_tab === 1 &&
                <Grid item xs={12}>
                    <Typography variant="h6">
                        Stok Durumum
                    </Typography>
                    {this.state.stoklar.length === 0 ?
                        <Typography variant="subtitle1">
                            Stok bilgisi bulunamadı. Stok durumunuzu sisteme ekleyerek takibini yapabilirsiniz.
                        </Typography>
                        :
                        <table>
                            <tbody>
                            <tr>
                                <th>
                                    <Typography variant="subheading">
                                        Ürün Adı
                                    </Typography>
                                </th>
                                <th>
                                    <Typography variant="subheading">
                                        Miktar
                                    </Typography>
                                </th>
                                <th>
                                    <Typography variant="subheading">
                                        Önerilen Miktar
                                    </Typography>
                                </th>
                                <th>
                                    <Typography variant="subheading">
                                        Durum
                                    </Typography>
                                </th>
                            </tr>
                            {this.state.stoklar.map(
                                stok => {
                                    return <tr key={stok.id}>
                                        <td style={{paddingLeft: '10px', paddingRight: '10px'}}>
                                            <Typography variant="subheading">
                                                {stok.urun_adi}
                                            </Typography>
                                        </td>
                                        <td style={{paddingLeft: '10px', paddingRight: '10px'}}>
                                            <Typography variant="subheading">
                                                {stok.miktar}
                                            </Typography>
                                        </td>
                                        <td style={{paddingLeft: '10px', paddingRight: '10px'}}>
                                            <Typography variant="subheading">
                                                {this.getOnerilenStok(stok.urun_id)}
                                            </Typography>

                                        </td>
                                        <td>
                                            {stok.miktar < this.getOnerilenStok(stok.urun_id) - 2
                                                ?
                                                <Typography variant="subheading" color="error">
                                                    YETERSİZ STOK
                                                </Typography>
                                                :
                                                (
                                                    stok.miktar > this.getOnerilenStok(stok.urun_id) - 2 && stok.miktar < this.getOnerilenStok(stok.urun_id) + 5
                                                        ?
                                                        <MuiThemeProvider theme={theme}>
                                                            <Typography variant="subheading" color="primary">
                                                                UYGUN
                                                            </Typography>
                                                        </MuiThemeProvider>
                                                        :
                                                        <MuiThemeProvider theme={theme}>
                                                            <Typography variant="subheading" color="secondary">
                                                                İHTİYAÇ FAZLASI STOK
                                                            </Typography>
                                                        </MuiThemeProvider>

                                                )

                                            }

                                        </td>
                                        <td><UrunStokGuncelleModal stokId={stok.id}
                                                                   miktar={stok.miktar}
                                                                   parentUpdate={() => {
                                                                       this.sendStokTavsiyeRequest();
                                                                       this.sendStokListRequest();
                                                                   }}/>
                                        </td>
                                        <td><UrunStokSilModal stokId={stok.id}
                                                              parentUpdate={() => {
                                                                  this.sendStokTavsiyeRequest();
                                                                  this.sendStokListRequest();
                                                              }}/>
                                        </td>
                                    </tr>
                                }
                            )}
                            </tbody>

                        </table>
                    }
                </Grid>
                }

            </Grid>
        </div>

    }

}


export default withCookies(LayoutBayi);