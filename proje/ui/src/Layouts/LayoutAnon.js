import React, {Component} from 'react';
import axios from 'axios';
import logo from '../Images/logo-updated.png'
import {withCookies} from 'react-cookie';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import './Layout.css'

class LayoutAnon extends Component {

    state = {
        username: '',
        password: ''
    };

    onChangeUsername = (event) => {
        this.setState({
            username: event.target.value,
            username_valid_text: ''
        });
    };

    onChangePassword = (event) => {
        this.setState({
            password: event.target.value
        });
    };

    sendLoginRequest = () => {

        var requestData = {
            'username': this.state.username,
            'password': this.state.password
        };

        var config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };

        axios.post('http://127.0.0.1:8000/bayilogin/', requestData, config)
            .then(response => {

                const updateStateData = {
                    is_logged_in: true,
                    bayi_id: response.data.bayi_id,
                    bayi_username: response.data.bayi_username,
                    bayi_unvan: response.data.bayi_unvan,
                    bayi_aciklama: response.data.bayi_aciklama,
                    bayi_adres_id: response.data.bayi_adres_id,
                    bayi_bakiye: response.data.bayi_bakiye,
                };

                // update app state on login success
                this.props.loginStateUpdater(updateStateData);
            })
            .catch(error => {

                console.log(error.response);
            });

    };

    render() {

        return (
            <div className="anonymous-layout-container">
                <div>
                    <img src={logo} style={{width: '200px'}}
                         alt="app logo"
                    />
                </div>


                <br/>

                <div>
                    <TextField
                        variant="outlined"
                        id="username_login"
                        label="Kullanıcı Adı"
                        value={this.state.username}
                        onChange={this.onChangeUsername}
                        margin="normal"
                    />
                    <br/>
                    <TextField
                        variant="outlined"
                        id="password_login"
                        label="Şifre"
                        value={this.state.password}
                        onChange={this.onChangePassword}
                        type="password"
                        margin="normal"
                    />
                    <br/>

                    <Button disabled={this.state.username === '' || this.state.password === ''}
                            variant="contained"
                            color="primary"
                            onClick={() => this.sendLoginRequest()}>
                        GİRİŞ
                    </Button>
                </div>
            </div>
        )

    }

}


export default withCookies(LayoutAnon);