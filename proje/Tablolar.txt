Bayi
	unvan
	adres_id
	aciklama
	bakiye

Urun
	urun_adi
	aciklama
	resim_url

Stok
	bayi_id
	urun_id
	miktar

Adres
	sehir
	ilce
	acik_adres
	enlem
	boylam

Musteri
	adres_id
	isim
	musteri_kodu
	yas
	cinsiyet

Sepet
	alan_bayi_id
	teslimat_bayi_id
	musteri_id 			(zorunlu degil)
	tekil_kod
	durum				(siparis alindi, kargoda, gönderildi)
	teslimat_adresi_id
	note 				(text)
	siparis_tarihi		(siparisin verildigi tarih)
	teslimat_tarihi		(teslim edilmesi istenen tarih)
	hedef_musteri_id
	
SepetKalemleri
	sepet_id
	urun_id
	miktar
	tutar
